<?php


use App\Http\Controllers\ClientController;

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;

use App\Http\Controllers\ImageController;
use App\Http\Controllers\MailController;

use App\Http\Controllers\ThemeController;
use App\Models\Mail;
use Asseco\Inbox\Contracts\CanMatch;
use Asseco\Inbox\Inbox;
use BeyondCode\Mailbox\Facades\Mailbox;
use BeyondCode\Mailbox\InboundEmail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    Route::get('/', [HomeController::class, 'index'])->name("home");
    Route::get('/request-service', [HomeController::class, 'request'])->name("request-service");

});


Route::middleware(['auth:sanctum', 'verified'])->prefix(LaravelLocalization::setLocale() . '/admin')->group(function () {


    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('change-theme/{color}', ThemeController::class)->name('change.theme');
    Route::get('images/index', [ImageController::class, 'index'])->name('image.index');
    Route::post('images/update', [ImageController::class, 'update'])->name('image.update');
    Route::prefix('clients')->name('clients.')->group(function () {
        Route::get('data-tables', [ClientController::class, 'datatables'])->name('datatables');
        Route::put('/{id}/main-details/update', [ClientController::class, 'updateClientDetails'])->name('main-details.update');
    });
    //


    Route::get('all/data-tables', [MailController::class, 'datatables'])->name('mails.datatables');
    Route::post('mails/send_message', [MailController::class, 'send'])->name('mails.send');
    Route::get('all-meals', [MailController::class, 'mails'])->name('mails.index');
    Route::put('setting/mails', [MailController::class, 'settingUpdate'])->name('setting.mail.update');
    Route::get('setting/mails/{type}', [MailController::class, 'setting'])->name('setting.mail');
    Route::get('mail/new', [MailController::class, 'create'])->name('mails.create');





});





/*Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');*/
Route::get("data", function () {
    $client = Webklex\IMAP\Facades\Client::account('default');

//Connect to the IMAP Server
    $client->connect();

//Get all Mailboxes

    $folders = $client->getFolders();
    $folders = $client->getFolder('All Mail');

    //Get all Messages of the current Mailbox $folder
    /** @var \Webklex\PHPIMAP\Support\MessageCollection $messages */
    $messages = $folders->messages()->all()->limit($limit = 10, $page = 1)->get();


});
