<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

use LaravelLocalization;

/**
 * App\Models\BaseModel
 *
 * @property-read mixed $name
 * @property-read mixed $title
 * @method static Builder|BaseModel newModelQuery()
 * @method static Builder|BaseModel newQuery()
 * @method static Builder|BaseModel query()
 * @mixin \Eloquent
 * @method static Builder|BaseModel enabled()
 */
class BaseModel extends Model
{
    public const STATUS_ENABLED = true;

    public const STATUS_DISABLED = false;
    public  const  STATUS_ACTIVE = 1;

    public const STATUS_INACTIVE = 0;

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        $names = [
            'ar' => $this->name_ar,
            'en' => $this->name_en,
            'de' => $this->name_de,
        ];
        return $names[app()->getLocale()];
    }
    public function getTitleAttribute()
    {
        $titles = [
            'ar' => $this->title_ar,
            'en' => $this->title_en,
            'de' => $this->title_de,
        ];
        return $titles[LaravelLocalization::getCurrentLocale()];
    }
    public function getBodyAttribute()
    {
        $titles = [
            'ar' => $this->body_ar,
            'en' => $this->body_en,
            'de' => $this->body_de,
        ];
        return $titles[LaravelLocalization::getCurrentLocale()];
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeEnabled($query)
    {
        return $query->where('status', 1);
    }

}
