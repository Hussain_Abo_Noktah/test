<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    use HasFactory;
    static $SERVICE_IMAGE = "FILE";
    static $SEND = "send";
    static $INBOX = "inbox";

    protected $fillable = ['from' ,'to','name','title' ,'subject','body','type'];

}
