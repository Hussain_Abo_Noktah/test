<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property string $first_name
 * @property string $family_name
 * @property string $mobile
 * @property string $email
 * @property int $age
 * @mixin \Eloquent
 * @property string $status
 */
class Client extends BaseModel
{
    use HasFactory;

    protected $fillable = ['first_name' ,'family_name' ,'mobile' , 'email' , 'age'];




    public function  client_details(){
        return $this->hasOne(ClientDetails::class);
    }

}
