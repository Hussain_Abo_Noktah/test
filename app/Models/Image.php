<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Image extends Model implements HasMedia
{
    use HasFactory ,  InteractsWithMedia;


    protected $fillable =['type'];

    /**
     * @var string
     */
    public static string $SERVICE_IMAGE = 'Service_Image';
    /**
     * @var string
     */
    public static string $BODY_IMAGE = 'Body_Image';


    /**
     * @var int $PROFILE_IMAGE_WIDTH
     */
    public static int $SERVICE_IMAGE_WIDTH = 629;

    /**
     * @var int $PROFILE_IMAGE_HEIGHT
     */
    public static int $BODY_IMAGE_HEIGHT = 1200;
  /**
     * @var int $PROFILE_IMAGE_WIDTH
     */
    public static int $BODY_IMAGE_WIDTH = 1072;

    /**
     * @var int $PROFILE_IMAGE_HEIGHT
     */
    public static int $SERVICE_IMAGE_HEIGHT = 647;

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::$SERVICE_IMAGE)->singleFile();
        $this->addMediaCollection(self::$BODY_IMAGE)->singleFile();
    }
    public function getServiceImageAttribute(){
        return $this->hasMedia(self::$SERVICE_IMAGE) ? $this->getFirstMediaUrl(self::$SERVICE_IMAGE) : null;
    }
    public function getBodyImageAttribute(){
        return $this->hasMedia(self::$BODY_IMAGE) ? $this->getFirstMediaUrl(self::$BODY_IMAGE) : null;
    }

}
