<?php

/**
 * @param $string
 * @param string $separator
 * @return string|string[]|null
 */


use App\Models\Setting;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

if (!function_exists('setSlug')) {
    function setSlug($string, $separator = '-')
    {
        if (is_null($string)) {
            return "";
        }
        $string = trim(htmlspecialchars($string));
        $string = strtolower(str_replace(['#', '/', '\\', ' ', '\'', '"', '`'], '-', $string));
        $string = mb_strtolower($string, "UTF-8");;
        $string = preg_replace("/[^a-z0-9_\sءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]#u/", "", $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        $string = preg_replace("/[\s_]/", $separator, $string);
        return $string;
    }
}


function status($status): bool
{
    return $status == "on" || 1 ? true : false;
}

if (!function_exists('setFileName')) {
    function setFileName($file): string
    {
        $name = explode('.', $file->getClientOriginalName());
        return time() . '-' . \Illuminate\Support\Str::random(100) . '.' . $file->extension();
    }
}

/**
 * @param $str
 * @return string
 */
if (!function_exists('htmlToPlainText')) {
    function htmlToPlainText($str): string
    {
        $str = str_replace('&nbsp;', ' ', $str);
        $str = html_entity_decode($str, ENT_QUOTES | ENT_COMPAT, 'UTF-8');
        $str = html_entity_decode($str, ENT_HTML5, 'UTF-8');
        $str = html_entity_decode($str);
        $str = htmlspecialchars_decode($str);
        $str = strip_tags($str);
        return $str;
    }
}

/**
 * @param $text
 * @param int $limit
 * @return string
 */
if (!function_exists('limit')) {
    function limit($text, $limit = 50): string
    {
        return Str::limit($text, $limit, '...');
    }
}


/**
 * @param $permission
 */
if (!function_exists('abort_permission')) {
    function abort_permission($permission)
    {
        if (request()->wantsJson()) {
            return response()->json([
                'success' => false,
                'message' => 'Permission Denied',
            ], 403);
        }
        abort_unless(Gate::allows($permission), 403);
    }
}

/**
 * @param $url
 * @return bool
 */
if (!function_exists('active_aside')) {
    function active_aside($url): bool
    {
        return str_contains(url()->current(), $url);
    }
}

/**
 * @return string
 */
if (!function_exists('dropZoneTempUrl')) {
    function dropZoneTempUrl(): string
    {
        return storage_path('app/public/temp/');
    }
}


/**
 * @param $requestInstance
 * @return bool
 */
if (!function_exists('RequestInUpdate')) {
    function RequestInUpdate($requestInstance): bool
    {
        return in_array(strtolower($requestInstance->getMethod()), ['put', 'patch']);
    }
}


if (!function_exists('price')) {
    function price($price): ?string
    {
        return DB::table('settings')
                ->first()->currency_symbol . ' ' . number_format($price ?? 0, 2);
    }
}

function settings($key)
{
    static $settings;

    if(is_null($settings))
    {
        $settings = Cache::remember('settings', 24*60, function() {
            return array_pluck(Setting::all()->toArray(), 'value', 'key');
        });
    }

    return (is_array($key)) ? array_only($settings, $key) : $settings[$key];
}







