<?php


namespace App\Http\services;


use App\Jobs\SendEmailJob;
use App\Mail\TestMail;
use App\Models\Setting;
use Mail;

class MailService extends BaseService
{


    public function sendMail($data)
    {
        try {

            $data['from'] = config('settings.mail_from_address');
            $data['from.name'] = config('settings.mail_from_name');

            dispatch(new SendEmailJob($data));
            $data = array(
                'res' => true,
                'message' => "The mail was sent"
            );
            return $data;
        } catch (\Exception $exception) {
            return array(
                'res' => false,
                'message' => $exception->getMessage()
            );
        }
    }
}
