<?php

namespace App\View\Components\Datatable;

use Illuminate\View\Component;

class Script extends Component
{
    public $route;
    public $columns;
    /**
     * @var bool
     */
    public $enableId;

    /**
     * Create a new component instance.
     *
     * @param $route
     * @param $columns
     * @param bool $enableId
     */
    public function __construct($route, $columns, $enableId = false)
    {
        //
        $this->route = $route;
        $this->columns = $columns;
        $this->enableId = $enableId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.datatable.script');
    }
}
