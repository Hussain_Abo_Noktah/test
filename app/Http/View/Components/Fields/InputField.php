<?php

namespace App\View\Components\Fields;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;

class InputField extends Component
{
    public ?string $col;
    public ?string $name;
    public ?string $title;
    public ?string $required;
    public ?string $type;
    public ?Model $model;

    /**
     * Create a new component instance.
     *
     * @param string|null $col
     * @param string|null $name
     * @param string|null $title
     * @param string|null $required
     * @param string|null $type
     * @param Model|null $model
     */
    public function __construct(
        ?string $col = null,
        ?string $name = null,
        ?string $title = null,
        ?string $required = null,
        ?string $type = null,
        ?Model $model = null
    )
    {
        //
        $this->col = $col;
        $this->name = $name;
        $this->title = $title;
        $this->required = $required;
        $this->type = $type;
        $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.fields.input-field');
    }
}
