<?php

namespace App\View\Components\Fields;

use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;
use Illuminate\View\View;

class FileField extends Component
{
    public ?string $title;
    public ?string $subTitle;
    public bool $required;
    public bool $multi;
    public ?string $name;
    public ?string $collection;
    public ?Model $model;

    /**
     * Create a new component instance.
     *
     * @param string|null $name
     * @param string|null $title
     * @param string|null $subTitle
     * @param bool $required
     * @param bool $multi
     * @param string|null $collection
     * @param Model|null $model
     */
    public function __construct(
        ?string $name = null,
        ?string $title = null,
        ?string $subTitle = null,
        bool $required = false,
        bool $multi = false,
        ?string $collection = null,
        ?Model $model = null
    )
    {
        //
        $this->title = $title;
        $this->subTitle = $subTitle;
        $this->required = $required;
        $this->name = $name;
        $this->multi = $multi;
        $this->collection = $collection;
        $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.fields.file-field');
    }
}
