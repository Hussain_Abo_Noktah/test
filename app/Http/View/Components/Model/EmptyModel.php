<?php

namespace App\View\Components\Model;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class EmptyModel extends Component
{
    public $id;

    /**
     * Create a new component instance.
     *
     * @param $id
     */
    public function __construct($id)
    {
        //
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.model.empty-model');
    }
}
