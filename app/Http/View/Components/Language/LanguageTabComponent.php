<?php

namespace App\View\Components\Language;

use Illuminate\View\Component;
use Illuminate\View\View;

class LanguageTabComponent extends Component
{
    /**
     * @var string|null
     */
    public ?string $id;

    /**
     * Create a new component instance.
     *
     * @param string|null $id
     */
    public function __construct(?string $id = null)
    {
        /** @var string $id */
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.language.language-tab');
    }
}
