<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Base\AdminController;
use App\Http\Requests\MailRequest;
use App\Http\services\MailService;
use App\Models\Client;

use App\Models\Mail;
use App\Models\Setting;
use App\Support\Icons;
use Illuminate\Http\Request;

class MailController extends AdminController
{


    protected $mailService;

    public function __construct(MailService $mailService)
    {

        $this->mailService = $mailService;
    }

    public function datatables(Request $request)
    {


        $mails = Mail::query()->where('type', $request->get('type'));
        return datatables()->of($mails)
            ->addColumn('date', function (Mail $mail) {
                return $mail->created_at->todatestring();
            })->addColumn('action', function (Mail $mail) {
                return '';
            })->rawColumns(['action', 'body'])->make(true);
    }


    public function mails()
    {



        try{
            $client =\Webklex\IMAP\Facades\Client::account('default');


            $client->connect();
            $folders = $client->getFolder('All Mail');

            $messages = $folders->messages()->all()->limit($limit = 10, $page = 1)->get();
            $paginator = $messages->paginate($per_page = 10, $page = null, $page_name = 'imap_page');

        } catch (\Exception $exception){
            session()->flash('error', $exception->getMessage());
            return back();
        }

        return view('admin.pages.mail.allMails')->with('paginator', $paginator);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = '')
    {
        //

        $columns = [
            'from',

            'name',
            'subject',
            'title',
            'body',
            'date',

        ];

        if ($type == Mail::$SEND) {
            array_push($columns, 'to');
        }

        return view('admin.pages.mail.' . $type)->with(['columns' => $columns]);
    }


    public function create()
    {
        return view('admin.pages.mail.create');
    }


    public function setting($type = '')
    {


        foreach (Setting::all() as $item) {
            $setting[$item->key] = $item->value;
        }
        return view('admin.pages.mail.' . $type)->with('setting', $setting);
    }

    public function settingUpdate(Request $request)
    {

        // $input = $request->all();


        $input = $request->except('_method', '_token', 'type');

        $keys = array_keys($input);
        // $validate = $request->validate('Base')

        foreach ($input as $key => $value) {
            try {
                Setting::where('key', $key)->first()->updateOrFail([
                    'key' => $key,
                    'value' => $value,
                ]);
            } catch (\Throwable $e) {
                session()->flash('error', $e->getMessage());
            }
        }

        foreach (Setting::all() as $item) {
            $setting[$item->key] = $item->value;
        }
        session()->flash('success', 'Update Successful');
        return back()->with('setting', $setting);
    }

    public function send(MailRequest $request)
    {
        try {
            $details = $request->except('_token', '_method');
            config()->set('mail.from.name', $request->get('name'));
            $res = $this->mailService->sendMail($details);
            session()->flash('success', 'Send Email Successed');

            $input = $request->except('_token', '_method', 'email');
            $input['from'] = config('settings.mail_from_address');
            $input['to'] = $request->get('email');
            Mail::query()->create($input);
            return redirect()->route('mails.create');
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            die();
        }


    }

}
