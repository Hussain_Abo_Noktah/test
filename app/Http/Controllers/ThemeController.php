<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param $color
     * @return RedirectResponse
     */
    public function __invoke(Request $request, $color): RedirectResponse
    {
        if (!in_array($color, ['dark', 'light'])) {
            abort(404);
        }
        session()->forget('theme_color');
        session()->put('theme_color', $color);
        return back();
    }
}
