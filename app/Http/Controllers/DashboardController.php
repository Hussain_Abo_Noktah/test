<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Models\User;
use App\Models\Order;

class DashboardController extends Controller
{

    /**
     * DashboardController constructor.
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * @return Application|Factory|View
     */

    public function index()
    {
        $clients = User::query()->count();
        $vendors = User::query()->count();

        $all_orders = User::query()->count();
        $pending_orders = User::query()->count();
        $on_the_way_orders = User::query()->count();
        $completed_orders = User::query()->count();
        return view('admin.pages.dashboard.index', compact('clients', 'vendors', 'all_orders', 'pending_orders', 'on_the_way_orders', 'completed_orders'));
    }


}
