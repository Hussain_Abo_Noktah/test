<?php


namespace App\Http\Controllers;


use App\Models\BaseModel;
use App\Models\Contact;
use App\Models\HomeSlider;
use App\Models\Image;
use App\Models\Section;
use App\Models\Service;

class HomeController extends Controller
{


    public function index()
    {

        return redirect()->route('login');
    }


}
