<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Base\AdminController;
use App\Models\Client;
use App\Models\ClientDetails;
use App\Models\RequestService;
use App\Support\Icons;
use Illuminate\Http\Request;

class ClientController extends AdminController
{


    public function datatables(Request $request)
    {
        $clients = Client::all();
        return datatables()->of($clients)
            ->addColumn('date', function (Client $client) {
                return $client->created_at->todatestring();
            })->addColumn('status', function (Client $client) {
                return __($client->status);
            })->addColumn('action', function (Client $client) {
                $html = '<a href="' . route('clients.edit', $client->id) . '" class="btn btn-sm btn-clean btn-icon mr-2">' . Icons::EditIcon() . '</a>';
                $html .= '<a href="' . route('clients.show', $client->id) . '" class="btn btn-sm btn-clean btn-icon mr-2">' . Icons::ShowIcon() . '</a>';
                return $html;
            })->rawColumns(['action'])
            ->make(true);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $columns = [

            'first_name',
            'family_name',
            'email',
            'mobile',
            'age',
            'date',
            'status'
        ];
        return view('admin.pages.client.index')->with(['columns' => $columns]);
    }

    public function edit($id)
    {
        $client = Client::find($id);

        return view('admin.pages.client.edit')->with([
            'client' => $client,
        ]);
    }


    public function show($id)
    {

        $client = Client::find($id);

        return view('admin.pages.client.show')->with(['client' => $client]);

    }


    public function updateClientDetails($id, Request $request)
    {
        $client = Client::find($id);

        if (!$client) {
            session()->flash('error', __('The Data is Invalid'));
            return back();

        }
        $client_details = $client->client_details;

        if (!$client_details) {
            $client_details = new ClientDetails;
            $client_details->client_id = $id ;
        }


        try {
            $client_details->fill($request->only('address', 'city', 'zip_code',
                'country', 'street','house_number', 'born_place', 'birthday'
            ));

            $res = $client_details->save();
            if ($res) {
                session()->flash('success', __('Update Main Client Details'));
            }
        } catch (\Exception $e){
            session()->flash('error', __($e->getMessage()));
        }

        return back();
    }
}
