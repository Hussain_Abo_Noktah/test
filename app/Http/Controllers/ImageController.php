<?php


namespace App\Http\Controllers;


use App\Models\Image;
use App\Support\ResizeImage;
use Illuminate\Http\Request;

class ImageController extends  Controller
{


    public function index(){
        $images = Image::all();
        return view('admin.pages.home.edite')->with(['images' => $images]);
    }


    public function update(Request $request){



        if ($request->hasFile('Service_Image')) {

            $image = Image::where('type', 'Service_Image')->first();

            if ($image){
                $profile_image = (new ResizeImage())->uploadImage($request->Service_Image, Image::$SERVICE_IMAGE_WIDTH, Image::$SERVICE_IMAGE_HEIGHT);
                $image->addMedia($profile_image)->toMediaCollection(Image::$SERVICE_IMAGE);


            }
         }
        if ($request->hasFile('Body_Image')) {

            $image = Image::where('type', 'Body_Image')->first();

            if ($image){
                $profile_image = (new ResizeImage())->uploadImage($request->Body_Image, Image::$BODY_IMAGE_WIDTH, Image::$BODY_IMAGE_HEIGHT);
                $image->addMedia($profile_image)->toMediaCollection(Image::$BODY_IMAGE);

            }
        }

        return redirect()->route('image.index')->with(__('Image Updated Successfully'));

    }
}
