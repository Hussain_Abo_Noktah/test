<?php


namespace App\Http\Repositories;



use App\Core\Base\BaseRepository;
use App\Models\Contact;
use App\Models\Section;


class ContactRepository extends  BaseRepository
{
    public function __construct(Contact $model) {
        parent::__construct($model);
    }
}
