<?php

namespace App\Providers;

use App\Models\Mail;
use App\Models\Setting;
use App\View\Components\Buttons\BackButtonComponent;
use App\View\Components\Card\CardBody;
use App\View\Components\Card\CardContent;
use App\View\Components\Card\CardFooter;
use App\View\Components\Card\CardHeader;
use App\View\Components\Card\CardTitle;
use App\View\Components\Card\CardToolbar;
use App\View\Components\Datatable\HtmlStructure;
use App\View\Components\Fields\InputField;
use App\View\Components\Fields\SelectField;
use App\View\Components\Fields\TextField;
use App\View\Components\Icons\Layout\Svg\Layout4Block;
use App\View\Components\Language\LanguageStructure;
use App\View\Components\Language\LanguageTabComponent;
use App\View\Components\Model\EmptyModel;
use App\View\Components\Model\ModelEmptyButton;
use BeyondCode\Mailbox\Facades\Mailbox;
use BeyondCode\Mailbox\InboundEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        Schema::defaultStringLength(125);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Model::unguard();

        Blade::component(Layout4Block::class, 'svg.4-block-icon');
        Blade::component(CardContent::class, 'card-content');
        Blade::component(CardHeader::class, 'card-header');
        Blade::component(CardTitle::class, 'card-title');
        Blade::component(CardFooter::class, 'card-footer');
        Blade::component(CardToolbar::class, 'card-toolbar');
        Blade::component(CardBody::class, 'card-body');
        Blade::component(HtmlStructure::class, 'datatables-html');
        Blade::component(LanguageStructure::class, 'language-structure');
        Blade::component(ModelEmptyButton::class, 'add-button');
        Blade::component(EmptyModel::class, 'empty-model');
        Blade::component(InputField::class, 'input-field');
        Blade::component(TextField::class, 'text-field');
        Blade::component(SelectField::class, 'select-field');
        Blade::component(BackButtonComponent::class, 'back-btn');
        Blade::component(LanguageTabComponent::class, 'languages-tab');
        session()->put('theme_color', 'dark');

        if (Schema::hasTable('settings')) {
            $settings = Setting::all();
            foreach ($settings as $setting) {
                Config::set('settings.' . $setting->key, $setting->value);
                config(['settings.' . $setting->key => $setting->value]);
            }
            config(['services.mailgun.domain' => config('settings.mailgun_secret')]);
            config(['services.mailgun.secret' => config('settings.mailgun_domain')]);
            config(['services.postmark.token' => config('settings.postmark_token')]);


            \config()->set('mail', array_merge(config('mail'), [
                'transport' => config('settings.mail_driver'),
                'host' => config('settings.mail_host'),
                'port' => config('settings.mail_port'),
                'encryption' => config('setting.mail_encryption', 'tls'),
                'username' => config('settings.mail_username'),
                'from.name' => config('settings.mail_from_name'),
                'from.address' => config('settings.mail_from_address'),
                'password' => config('settings.mail_password'),
            ]));
            \config()->set('imap.accounts.default.host' , config('settings.mail_host'));
            \config()->set('imap.accounts.default.port' , config('settings.mail_port'));
            \config()->set('imap.accounts.default.encryption' , config('settings.mail_encryption'));
            \config()->set('imap.accounts.default.username' , config('settings.mail_username'));
            \config()->set('imap.accounts.default.password' , config('settings.mail_password'));

        }

        }
    }
