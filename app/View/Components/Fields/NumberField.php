<?php

namespace App\View\Components\Fields;

use Illuminate\View\Component;
use Illuminate\View\View;

class NumberField extends Component
{
    public string $title;
    public $model;
    public bool $required;

    /**
     * Create a new component instance.
     *
     * @param string $title
     * @param null $model
     * @param bool $required
     */
    public function __construct(string $title, $model = null, $required = false)
    {
        $this->title = $title;
        $this->model = $model;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.fields.number-field');
    }
}
