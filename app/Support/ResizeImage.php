<?php


namespace App\Support;

use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ResizeImage
{

    public $thumbnailPath;

    /**
     * @var string
     */
    public string $folderName;

    /**
     * @var string
     */
    public string $ds = '/';

    public function __construct()
    {
        $this->thumbnailPath = null;
        $this->folderName = 'custom-temp';
        $this->ds = DIRECTORY_SEPARATOR;
    }

    /**
     * @function CreateDirectory
     * create required directory if not exist and set permissions
     */
    protected function createDirectory()
    {
        $paths = [
            'thumbnail_path' => storage_path('app' . $this->ds . 'public' . $this->ds . $this->folderName)
        ];
        foreach ($paths as $key => $path) {
            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            }
        }
        $this->thumbnailPath = $paths['thumbnail_path'];
    }

    /**
     * @param $originalImage
     * @param int|null $width
     * @param null $height
     * @param int $quality
     * @param bool $aspectRatio
     * @return string
     * @throws Exception
     */
    public function uploadImage($originalImage, int $width = null, $height = null, int $quality = 90, $aspectRatio = false): string
    {
        if ($width == null && $height == null) {
            throw new Exception('Missing Width Or Height Parameter');
        }
        $this->createDirectory();
      //  $fileName = setFileName($originalImage);
     $name = explode('.', $originalImage->getClientOriginalName());
        $fileName =  time() . '-' . \Illuminate\Support\Str::random(100) . '.' . $originalImage->extension();
        $path = $this->thumbnailPath . $this->ds . $fileName;
        try {
            if ($aspectRatio == true) {
                Image::make($originalImage)->resize($width, $height, function ($ratio) {
                    $ratio->aspectRatio();
                })->save($path, $quality);
            } else {
                Image::make($originalImage)->resize($width, $height)->save($path, $quality);
            }
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }
        return $this->thumbnailPath . $this->ds . $fileName;
    }

}
