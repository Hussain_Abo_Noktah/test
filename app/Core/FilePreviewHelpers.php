<?php



namespace App\Components\Core;

use URL;

trait FilePreviewHelpers
{
  public function generatePreviewPath($id, $width)
  {
    return URL::to('/files') . "/{$id}/preview?w={$width}&action=resize";
    //return URL::to('/files') . "/{$id}/preview?w=full";
  }
}
