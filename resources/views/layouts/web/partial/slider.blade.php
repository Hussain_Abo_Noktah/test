<section class="pos-relative main-header over-hidden" id="main-header">
    <div class="container-fluid p-0">
        <div class="carousel slide text-center" id="MainSlider" data-bs-ride="carousel">
            <div class="carousel-inner">
            @foreach($saphir_images as $key  => $image)
                    <div class="carousel-item @if($key == 1)  active bg-fx-img @endif"
                         style="background-image:url('{{$slider_images[$key]}}')">
                        <div class="w-100 content row p-0">
                            <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-10 offset-1 h-100 slide-item">
                                <div class="header-title white-text">
                                    <div
                                        class="f-bold pos-relative d-block main-title animate__animated animate__fadeInLeft text-uppercase">
                                        <h2 class="d-none">Saphir</h2>
                                        <div class="d-inline-block logo-name full">
                                            <img class="w-100"
                                                 src="{{asset('css/web/imgs/name/'. $image)}}">
                                        </div>
                                        <div class="d-inline-block logo-name move">
                                            <img class="w-100"
                                                 src="{{asset('css/web/imgs/name/'. $saphir_char[$key])}}">
                                        </div>
                                    </div>
                                    <a class="scroll-down animate__animated animate__fadeInUp" href="#main-statment">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="127"
                                             viewBox="0 0 50 127">
                                            <g data-name="Group 3" transform="translate(-910 -811)">
                                                <g data-name="Rectangle 2" transform="translate(910 811)" fill="none"
                                                   stroke="#fff" stroke-width="2">
                                                    <rect width="50" height="127" rx="25" stroke="none"/>
                                                    <rect x="1" y="1" width="48" height="125" rx="24" fill="none"/>
                                                </g>
                                                <g data-name="Icon feather-arrow-down" transform="translate(916.5 977.685)">
                                                    <path data-name="Path 4" d="M18,7.5V63.049"
                                                          transform="translate(0.2 -138.185)" fill="none" stroke="#fff"
                                                          stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    <path data-name="Path 5" d="M28.9,18,18.2,28.7,7.5,18"
                                                          transform="translate(0 -103.836)" fill="none" stroke="#fff"
                                                          stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!-- <div class="carousel-item bg-fx-img"
                     style="background-image:url({{asset('css/web/imgs/slides/2.jpg')}}">
                    <div class="w-100 content row p-0">
                        <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-10 offset-1 h-100 slide-item">
                            <div
                                class="f-bold pos-relative d-block main-title animate__animated animate__fadeInLeft text-uppercase">
                                <h2 class="d-none">Saphir</h2>
                                <div class="d-inline-block logo-name full">
                                    <img class="w-100"
                                         src="{{asset('css/web/imgs/name/saphir-a.png')}}">
                                </div>
                                <div class="d-inline-block logo-name move">
                                    <img class="w-100"
                                         src="{{asset('css/web/imgs/name/a.png')}}">
                                </div>
                                <a class="scroll-down animate__animated animate__fadeInUp" href="#main-statment">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="127"
                                         viewBox="0 0 50 127">
                                        <g data-name="Group 3" transform="translate(-910 -811)">
                                            <g data-name="Rectangle 2" transform="translate(910 811)" fill="none"
                                               stroke="#fff" stroke-width="2">
                                                <rect width="50" height="127" rx="25" stroke="none"/>
                                                <rect x="1" y="1" width="48" height="125" rx="24" fill="none"/>
                                            </g>
                                            <g data-name="Icon feather-arrow-down" transform="translate(916.5 977.685)">
                                                <path data-name="Path 4" d="M18,7.5V63.049"
                                                      transform="translate(0.2 -138.185)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                <path data-name="Path 5" d="M28.9,18,18.2,28.7,7.5,18"
                                                      transform="translate(0 -103.836)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item bg-fx-img"
                     style="background-image:url({{asset('css/web/imgs/slides/3.jpg')}})">
                    <div class="w-100 content row p-0">
                        <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-10 offset-1 h-100 slide-item">
                            <div
                                class="f-bold pos-relative d-block main-title animate__animated animate__fadeInLeft text-uppercase">
                                <h2 class="d-none">Saphir</h2>
                                <div class="d-inline-block logo-name full">
                                    <img class="w-100"
                                         src="{{asset('css/web/imgs/name/saphir-p.png')}}">
                                </div>
                                <div class="d-inline-block logo-name move">
                                    <img class="w-100"
                                         src="{{asset('css/web/imgs/name/p.png')}}">
                                </div>
                                <a class="scroll-down animate__animated animate__fadeInUp" href="#main-statment">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="127"
                                         viewBox="0 0 50 127">
                                        <g data-name="Group 3" transform="translate(-910 -811)">
                                            <g data-name="Rectangle 2" transform="translate(910 811)" fill="none"
                                               stroke="#fff" stroke-width="2">
                                                <rect width="50" height="127" rx="25" stroke="none"/>
                                                <rect x="1" y="1" width="48" height="125" rx="24" fill="none"/>
                                            </g>
                                            <g data-name="Icon feather-arrow-down" transform="translate(916.5 977.685)">
                                                <path data-name="Path 4" d="M18,7.5V63.049"
                                                      transform="translate(0.2 -138.185)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                <path data-name="Path 5" d="M28.9,18,18.2,28.7,7.5,18"
                                                      transform="translate(0 -103.836)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item bg-fx-img"
                     style="background-image:url({{asset('css/web/imgs/slides/1.jpg')}})">
                    <div class="w-100 content row p-0">
                        <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-10 offset-1 h-100 slide-item">
                            <div
                                class="f-bold pos-relative d-block main-title animate__animated animate__fadeInLeft text-uppercase">
                                <h2 class="d-none">Saphir</h2>
                                <div class="d-inline-block logo-name full"><img class="w-100"
                                                                                src="{{asset('css/web/imgs/name/saphir-h.png')}}">
                                </div>
                                <div class="d-inline-block logo-name move"><img class="w-100"
                                                                                src="{{asset('css/web/imgs/name/h.png')}}">
                                </div>
                                <a class="scroll-down animate__animated animate__fadeInUp" href="#main-statment">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="127"
                                         viewBox="0 0 50 127">
                                        <g data-name="Group 3" transform="translate(-910 -811)">
                                            <g data-name="Rectangle 2" transform="translate(910 811)" fill="none"
                                               stroke="#fff" stroke-width="2">
                                                <rect width="50" height="127" rx="25" stroke="none"/>
                                                <rect x="1" y="1" width="48" height="125" rx="24" fill="none"/>
                                            </g>
                                            <g data-name="Icon feather-arrow-down" transform="translate(916.5 977.685)">
                                                <path data-name="Path 4" d="M18,7.5V63.049"
                                                      transform="translate(0.2 -138.185)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                <path data-name="Path 5" d="M28.9,18,18.2,28.7,7.5,18"
                                                      transform="translate(0 -103.836)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item bg-fx-img"
                     style="background-image:url({{asset('css/web/imgs/slides/2.jpg')}})">
                    <div class="w-100 content row p-0">
                        <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-10 offset-1 h-100 slide-item">
                            <div
                                class="f-bold pos-relative d-block main-title animate__animated animate__fadeInLeft text-uppercase">
                                <h2 class="d-none">Saphir</h2>
                                <div class="d-inline-block logo-name full"><img class="w-100"
                                                                                src="{{asset('css/web/imgs/name/saphir-i.png')}}">
                                </div>
                                <div class="d-inline-block logo-name move"><img class="w-100"
                                                                                src="{{asset('css/web/imgs/name/i.png')}}">
                                </div>
                                <a class="scroll-down animate__animated animate__fadeInUp" href="#main-statment">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="127"
                                         viewBox="0 0 50 127">
                                        <g data-name="Group 3" transform="translate(-910 -811)">
                                            <g data-name="Rectangle 2" transform="translate(910 811)" fill="none"
                                               stroke="#fff" stroke-width="2">
                                                <rect width="50" height="127" rx="25" stroke="none"/>
                                                <rect x="1" y="1" width="48" height="125" rx="24" fill="none"/>
                                            </g>
                                            <g data-name="Icon feather-arrow-down" transform="translate(916.5 977.685)">
                                                <path data-name="Path 4" d="M18,7.5V63.049"
                                                      transform="translate(0.2 -138.185)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                <path data-name="Path 5" d="M28.9,18,18.2,28.7,7.5,18"
                                                      transform="translate(0 -103.836)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item bg-fx-img"
                     style="background-image:url({{asset('css/web/imgs/slides/3.jpg')}});">
                    <div class="w-100 content row p-0">
                        <div class="col-xl-6 offset-xl-3 col-lg-6 offset-lg-3 col-10 offset-1 h-100 slide-item">
                            <div
                                class="f-bold pos-relative d-block main-title animate__animated animate__fadeInLeft text-uppercase">
                                <h2 class="d-none">Saphir</h2>
                                <div class="d-inline-block logo-name full"><img class="w-100"
                                                                                src="{{asset('css/web/imgs/name/saphir-r.png')}}">
                                </div>
                                <div class="d-inline-block logo-name move"><img class="w-100"
                                                                                src="{{asset('css/web/imgs/name/r.png')}}">
                                </div>
                                <a class="scroll-down animate__animated animate__fadeInUp" href="#main-statment">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="127"
                                         viewBox="0 0 50 127">
                                        <g data-name="Group 3" transform="translate(-910 -811)">
                                            <g data-name="Rectangle 2" transform="translate(910 811)" fill="none"
                                               stroke="#fff" stroke-width="2">
                                                <rect width="50" height="127" rx="25" stroke="none"/>
                                                <rect x="1" y="1" width="48" height="125" rx="24" fill="none"/>
                                            </g>
                                            <g data-name="Icon feather-arrow-down" transform="translate(916.5 977.685)">
                                                <path data-name="Path 4" d="M18,7.5V63.049"
                                                      transform="translate(0.2 -138.185)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                <path data-name="Path 5" d="M28.9,18,18.2,28.7,7.5,18"
                                                      transform="translate(0 -103.836)" fill="none" stroke="#fff"
                                                      stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="row">
            <div class="col-10 offset-1">
                <div class="d-inline-block arrows">
                    <div class="d-inline-block next"><i class="fas fa-angle-right"></i></div>
                    <div class="d-inline-block prev"><i class="fas fa-angle-left"></i></div>
                </div>
            </div>
        </div>
    </div>
</section>
