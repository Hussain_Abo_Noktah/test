<section class="pos-relative white-text main-footer pt-5 pb-5 bg-fx-img" id="main-footer" style="background-image:url({{asset('css/web/imgs/footer.jpg')}})">
    <div class="before black-bg"></div>
    <div class="container pos-relative pt-5">
        <div class="row pt-5">
            <div class="col-lg-2 offset-lg-0 col-4 offset-4"><a class="w-100 foot-logo pe-5" href="{{route('home')}}"><img class="w-100 d-block pt-3 pb-3" src="{{asset('css/web/imgs/logo-w.png')}}" alt="Saphir"></a></div>
            <div class="col-lg-5 col-12 ps-lg-5">
                <div class="foot-links">
                    <a class="f-big f-med white-text ps-lg-0  pe-lg-5 pe-3 ps-3 pt-2 pb-2 d-inline-block second-hover" href="#" title="Profile">
                        <span class="animate__animated animate__slideInUp duration_2s">{{__('profile')}}</span>
                    </a>
                    <a class="f-big f-med white-text ps-lg-0  pe-lg-5 pe-3 ps-3 pt-2 pb-2 d-inline-block second-hover" href="#" title="Work">
                        <span class="animate__animated animate__slideInUp duration_2s">{{__('Work')}}</span>
                    </a>
                    <a class="f-big f-med white-text ps-lg-0  pe-lg-5 pe-3 ps-3 pt-2 pb-2 d-inline-block second-hover" href="#services" title="Services">
                        <span class="animate__animated animate__slideInUp duration_2s">{{__('Services')}}</span></a><a class="mt-4 f-extra-medium pt-3 pb-3 d-block butn white-butn bordered second-hover" href="request.html" title="Project Request"><span class="animate__animated animate__slideInUp duration_2s">Project Request</span></a><a class="d-none second-hover" href="#" title="العربية"><span class="animate__animated animate__slideInUp duration_2s">العربية</span></a>
                </div>
                <div class="newsletter">
                    <h3 class="f-big f-med white-text pe-5 pt-2 pb-2 mb-4 mt-lg-5 mt-4">{{__('Newsletter')}}</h3>
                    <h5 class="f-normal lh-30 mb-4">{{__('Newsletter Body')}}</h5>
                    <form class="newsletter-form">
                        <div class="form-container pos-relative">
                            <input class="form-control" type="email" name="news-email" placeholder="{{__('Enter Your Email')}}">
                            <input class="mt-lg-0 mt-4" type="submit" value="{{__('Subscribe')}}">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-1 col-12 full-info mt-lg-0 mt-4">
                <div class="address h5 f-light lh-35 mb-3">{{$data['contact']->address}}</div><a class="d-inline-block view-map h5 white-text" href="#">{{__('View The Map')}}</a>
                <div class="div mb-4 mt-4 white-bg d-block"></div>
                <div class="contact"><a class="d-block phone h5 mb-3 white-text" href="tel:{{$data['contact']->phone}}"><span>{{$data['contact']->phone}}</span></a><a class="d-block email h5 mb-3 white-text" href="mailto:info@saphir.com"><span>info@saphir.com</span></a></div>
                <div class="follow">
                    <a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="{{$data['contact']->facebook}}" data-bs-toggle="tooltip" title="facebook"><i class="fab fa-facebook-f"></i></a>
                    <a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="{{$data['contact']->twitter}}" data-bs-toggle="tooltip" title="twitter"><i class="fab fa-twitter"></i></a>
                    <a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="{{$data['contact']->instagram}}" data-bs-toggle="tooltip" title="instagram"><i class="fab fa-instagram"></i></a>
                    <a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="{{$data['contact']->youtube}}" data-bs-toggle="tooltip" title="youtube"><i class="fab fa-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="{{asset('js/web/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/animate.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenLite.min.js"></script>
<script type="text/javascript" src="{{asset('js/web/ScrollMagic.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/plugins/animation.gsap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/plugins/debug.addIndicators.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/aos.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/fontawesome.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/color-calendar.js')}}"></script>
<script type="text/javascript" src="{{asset('js/web/main.js')}}"></script>
<!--for ltr
-->
<script type="text/javascript" src="{{asset('js/web/ltr.js')}}"></script>
@yield('script')
