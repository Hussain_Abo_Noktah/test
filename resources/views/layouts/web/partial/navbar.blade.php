<div style="position:fixed;top:0;left:0;width:0;height:0;" id="scrollzipPoint"></div>
<div class="top-menu white-text animate__animated animate__fadeIn main-bg full-menu" id="mainTopMenu">
    <div class="d-block w-100 mt-5 wrap">
        <div class="row">
            <div class="col-lg-4 offset-lg-2 col-12">
                <a class="menu-link f-big f-bold d-block pe-3 pt-1 pb-1 mb-1"
                   href="#">Profile</a><a
                    class="menu-link f-big f-bold d-block pe-3 pt-1 pb-1 mb-1" href="#">Work</a><a
                    class="menu-link f-big f-bold d-block pe-3 pt-1 pb-1 mb-1" href="#services">Services</a><a
                    class="menu-link f-big f-bold d-block pe-3 pt-1 pb-1 mb-1" href="{{route('request-service')}}">Project
                    Request</a>
                @foreach($languages as $key =>$lang)
                    @if(LaravelLocalization::getCurrentLocale() != $key)
                        <a class="menu-link f-big f-bold d-block pe-3 pt-1 pb-1 mb-1"
                           href="{{LaravelLocalization::getLocalizedURL($key)}}">{{$lang}}</a>
                    @endif
                @endforeach
            </div>
            <div class="col-lg-4 col-12 full-info">
                <div class="address h5 f-light lh-35 mb-3">{{$data['contact']->address}} </div>
                <a class="d-inline-block view-map h5 white-text" href="#">View The Map</a>
                <div class="div mb-4 mt-4 white-bg d-block"></div>
                <div class="contact"><a class="d-block phone h5 mb-3 white-text" href="tel:{{$data['contact']->phone}}"><span>{{$data['contact']->phone}}</span></a><a
                        class="d-block email h5 mb-3 white-text"
                        href="mailto:info@saphir.com"><span>{{$data['contact']->email}}</span></a></div>
                <div class="follow">
                    <a class="d-inline-block h5 pe-4 me-3 second-hover pt-3 pb-3 white-text"
                       href="{{$data['contact']->facebook}}"
                       data-bs-toggle="tooltip" title="facebook"><i class="fab fa-facebook-f"></i></a>
                    <a class="d-inline-block h5 pe-4 me-3 second-hover pt-3 pb-3 white-text"
                       href="{{$data['contact']->twitter}}"
                       data-bs-toggle="tooltip" title="twitter"><i class="fab fa-twitter"></i></a>
                    <a class="d-inline-block h5 pe-4 me-3 second-hover pt-3 pb-3 white-text"
                       href="{{$data['contact']->instagram}}"
                       data-bs-toggle="tooltip" title="instagram"><i class="fab fa-instagram"></i></a>
                    <a class="d-inline-block h5 pe-4 me-3 second-hover pt-3 pb-3 white-text"
                       href="{{$data['contact']->youtube}}"
                       data-bs-toggle="tooltip" title="youtube"><i class="fab fa-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Route::getCurrentRoute()->getName() == 'home')
    <section class="pos-relative top-header" id="top-header">
        <div class="container-fluid fixed-top" id="topbar">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-6"><a class="d-block logo pt-4 pb-4"
                                                                     href="{{route('home')}}"><img
                                class="w-100 icon animate__animated animate__fadeIn"
                                src="{{asset('css/web/imgs/logo-icon-w.png')}}" alt="Saphir"><img class="w-100 text"
                                                                                                  src="{{asset('css/web/imgs/logo-text-w.png')}}"
                                                                                                  alt="Saphir"></a>
                    </div>
                    <div class="col-xl-3 offset-xl-7 col-lg-4 offset-lg-5 col-md-6 col-6"><span
                            class="open-menu fl-right mouse" data-target="#mainTopMenu"><span class="d-block menu-butn"
                                                                                              title="Menu"
                                                                                              data-bs-toggle="tooltip"></span></span>
                    </div>
                </div>
            </div>
        </div>
        @includeWhen( Route::getCurrentRoute()->getName() == 'home' ,'layouts.web.partial.slider' ,[])

    </section>
@else
    <section class="pos-relative white-bg top-header" id="top-header">
        <div class="container-fluid fixed-top darkbar" id="topbar">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                        <a class="d-block logo pt-4 pb-4"
                           href="{{route('home')}}"><img
                                class="w-100 icon animate__animated animate__fadeIn"
                                src="{{asset('css/web/imgs/logo-icon.png')}}"
                                alt="Saphir"><img class="w-100 text" src="{{asset('css/web/imgs/logo-text.png')}}"
                                                  alt="Saphir">
                            <img
                                class="w-100 icon icon-2 animate__animated animate__fadeIn"
                                src="{{asset('css/web/imgs/logo-icon-w.png')}}"
                                alt="Saphir">
                            <img class="w-100 text text-2"
                                 src="{{asset('css/web/imgs/logo-text-w.png')}}" alt="Saphir"></a>
                    </div>
                    <div class="col-xl-3 offset-xl-7 col-lg-4 offset-lg-5 col-md-6 col-6"><span
                            class="open-menu fl-right mouse" data-target="#mainTopMenu">
                            <span class="d-block menu-butn"
                                  title="Menu"
                                  data-bs-toggle="tooltip"></span></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
