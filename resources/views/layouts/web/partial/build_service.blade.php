@extends('layouts.web.app')


@section('content')
    <section class="pos-relative white-bg top-header" id="top-header">
        <div class="container-fluid fixed-top darkbar" id="topbar">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-6"><a class="d-block logo pt-4 pb-4" href="{{url('/')}}"><img class="w-100 icon animate__animated animate__fadeIn" src="{{asset('css/web/imgs/logo-icon.png')}}" alt="Saphir"><img class="w-100 text" src="{{asset('css/web/imgs/logo-text.png')}}" alt="Saphir"><img class="w-100 icon icon-2 animate__animated animate__fadeIn" src="imgs/logo-icon-w.png" alt="Saphir"><img class="w-100 text text-2" src="imgs/logo-text-w.png" alt="Saphir"></a></div>
                    <div class="col-xl-3 offset-xl-7 col-lg-4 offset-lg-5 col-md-6 col-6"><span class="open-menu fl-right mouse" data-target="#mainTopMenu"><span class="d-block menu-butn" title="Menu" data-bs-toggle="tooltip"></span></span></div>
                </div>
            </div>
        </div>
    </section>
    <section class="pos-relative main-statment white-bg pt=5 pb-5" id="main-statment">
        <div class="container pt-lg-5 pb-lg-5">
            <div class="row mt-lg-5 mb-lg-5">
                <div class="col-lg-4 offset-lg-1 col-md-6 col-12 over-hidden">
                    <h2 class="f-huge f-bold main-text title" data-aos="fade-up">Project Request</h2>
                </div>
                <div class="col-lg-5 offset-lg-1 col-md-6 col-12">
                    <h3 class="desc h5 lh-35">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="pos-relative request-projectt white-bg pb-5" id="request-projectt">
        <div class="container mb-5">
            <div class="row">
                <div class="col-12">
                    <div class="h6"><span class="second-text me-1">*</span><span>Required Fields</span></div>
                </div>
                <div class="col-12">
                    <h2 class="f-extramedium f-bold main-text mt-4 mb-5">Personal Information</h2>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <input class="form-control" type="text" required="required">
                        <label><span class="me-1">Full name</span><span class="second-text">*</span></label>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <input class="form-control" type="number" required="required">
                        <label><span class="me-1">Age</span><span class="second-text">*</span></label>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <input class="form-control" type="email" required="required">
                        <label><span class="me-1">Email</span><span class="second-text">*</span></label>
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group">
                        <input class="form-control" type="tel">
                        <label><span class="me-1">Phone</span></label>
                    </div>
                </div>
                <div class="col-12 mt-5">
                    <h2 class="f-extramedium f-bold main-text mt-4 mb-5">Project Information</h2>
                </div>
                <div class="col-lg-3 col-md-6 col-12 serv-item mouse">
                    <div class="w-100 wrap d-block">
                        <div class="img over-hidden d-inline-block"><img class="w-100" src="{{asset('css/web/imgs/services/1.png')}}" alt="Service Title" data-aos="zoom-in"></div>
                        <div class="title"><span class="d-inline-block">Service Title</span></div>
                        <div class="price"><span class="d-inline-block me-2">3000</span><span class="d-inline-block f-normal">sar</span></div>
                        <div class="select-butn w-100"><span class="select"> Select</span><span class="selected">Selected</span></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 serv-item mouse">
                    <div class="w-100 wrap d-block">
                        <div class="img over-hidden d-inline-block"><img class="w-100" src="{{asset('css/web/imgs/services/1.png')}}" alt="Service Title" data-aos="zoom-in"></div>
                        <div class="title"><span class="d-inline-block">Service Title</span></div>
                        <div class="price"><span class="d-inline-block me-2">3000</span><span class="d-inline-block f-normal">sar</span></div>
                        <div class="select-butn w-100"><span class="select"> Select</span><span class="selected">Selected</span></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 serv-item mouse">
                    <div class="w-100 wrap d-block">
                        <div class="img over-hidden d-inline-block"><img class="w-100" src="{{asset('css/web/imgs/services/1.png')}}" alt="Service Title" data-aos="zoom-in"></div>
                        <div class="title"><span class="d-inline-block">Service Title</span></div>
                        <div class="price"><span class="d-inline-block me-2">3000</span><span class="d-inline-block f-normal">sar</span></div>
                        <div class="select-butn w-100"><span class="select"> Select</span><span class="selected">Selected</span></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 serv-item mouse">
                    <div class="w-100 wrap d-block">
                        <div class="img over-hidden d-inline-block"><img class="w-100" src="{{asset('css/web/imgs/services/1.png')}}" alt="Service Title" data-aos="zoom-in"></div>
                        <div class="title"><span class="d-inline-block">Service Title</span></div>
                        <div class="price"><span class="d-inline-block me-2">3000</span><span class="d-inline-block f-normal">sar</span></div>
                        <div class="select-butn w-100"><span class="select"> Select</span><span class="selected">Selected</span></div>
                    </div>
                </div>
                <div class="col-12 mt-5">
                    <h2 class="f-extramedium f-bold main-text mt-4 mb-5">Date</h2>
                    <div class="mb-5 h4" id="viewPickedDate"></div>
                </div>
                <div class="col-12">
                    <div class="w-100" id="pickDate"></div>
                    <input type="hidden" id="pickedDate">
                </div>
                <div class="col-12 mt-5">
                    <h2 class="f-extramedium f-bold main-text mt-4 mb-5">Paid Method</h2>
                </div>
                <div class="col-12">
                    <div class="form-group select-wrap">
                        <select class="form-control" id="selectPaid">
                            <option value="1" data-parent="#paidTabs" data-tab="#paidNav-1">Credit Card</option>
                            <option value="2" data-parent="#paidTabs" data-tab="#paidNav-2">Bank transfer</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 mt-5 paid-tabs" id="paidTabs">
                    <div class="w-100 paid-tab active" id="paidNav-1">
                        <div class="w-100 row m-0">
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" required="required">
                                    <label><span class="me-1">Card Number</span><span class="second-text">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" required="required">
                                    <label><span class="me-1">CVV Code</span><span class="second-text">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group select-wrap">
                                    <select class="form-control not-empty" required="required">
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                    </select>
                                    <label><span class="me-1">Expiry Date</span><span class="second-text">*</span></label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" required="required">
                                    <label><span class="me-1">Name on Card</span><span class="second-text">*</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 paid-tab" id="paidNav-2">
                        <div class="w-100 row m-0">
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="w-100 pe-3 pt-3 h4"><span class="d-block f-bold second-text me-3 mb-3">Account NAme</span><span>Ahmed Mohammed Ahmed</span></div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="w-100 pe-3 pt-3 h4"><span class="d-block f-bold second-text me-3 mb-3">Bank NAme</span><span>Al Ahly</span></div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="w-100 pe-3 pt-3 h4"><span class="d-block f-bold second-text me-3 mb-3">Bank Account</span><span>24152454fdgfd4545645</span></div>
                            </div>
                            <div class="col-12 mt-5">
                                <div class="form-group file-wrap pt-lg-0 pt-5"><span class="ic">Select File</span>
                                    <input class="form-control m-0" type="file" required="required">
                                    <label>
                                        <div class="w-100 d-inline-block"><span class="me-1">Attach Bank Transfer File</span><span class="second-text">*</span></div>
                                        <div class="w-100 d-block">
                                            <div class="d-inline-block mt-3 mb-3 file-name"></div>
                                        </div>
                                    </label>
                                    <div class="mb-3 delete-icon d-none pos-relative"><i class="fas fa-trash red-text not"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-12">
                    <div class="form-group"><a class="w-100 butn main-butn f-medium pt-3 pb-3 mt-lg-5 mt-3" href="request-done.html">Confirm</a></div>
                </div>
            </div>
        </div>
    </section>
    <section class="pos-relative white-text main-footer pt-5 pb-5 bg-fx-img" id="main-footer" style="background-image:url({{asset('css/web/imgs/footer.jpg')}})">
        <div class="before black-bg"></div>
        <div class="container pos-relative pt-5">
            <div class="row pt-5">
                <div class="col-lg-2 offset-lg-0 col-4 offset-4"><a class="w-100 foot-logo pe-5" href="{{url('/')}}"><img class="w-100 d-block pt-3 pb-3" src="{{asset('css/web/imgs/logo-w.png')}}" alt="Saphir"></a></div>
                <div class="col-lg-5 col-12 ps-lg-5">
                    <div class="foot-links"><a class="f-big f-med white-text ps-lg-0  pe-lg-5 pe-3 ps-3 pt-2 pb-2 d-inline-block second-hover" href="#" title="Profile"><span class="animate__animated animate__slideInUp duration_2s">Profile</span></a><a class="f-big f-med white-text ps-lg-0  pe-lg-5 pe-3 ps-3 pt-2 pb-2 d-inline-block second-hover" href="#" title="Work"><span class="animate__animated animate__slideInUp duration_2s">Work</span></a><a class="f-big f-med white-text ps-lg-0  pe-lg-5 pe-3 ps-3 pt-2 pb-2 d-inline-block second-hover" href="#services" title="Services"><span class="animate__animated animate__slideInUp duration_2s">Services</span></a><a class="mt-4 f-extra-medium pt-3 pb-3 d-block butn white-butn bordered second-hover" href="request.html" title="Project Request"><span class="animate__animated animate__slideInUp duration_2s">Project Request</span></a><a class="d-none second-hover" href="#" title="العربية"><span class="animate__animated animate__slideInUp duration_2s">العربية</span></a>
                    </div>
                    <div class="newsletter">
                        <h3 class="f-big f-med white-text pe-5 pt-2 pb-2 mb-4 mt-lg-5 mt-4">Newsletter</h3>
                        <h5 class="f-normal lh-30 mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</h5>
                        <form class="newsletter-form">
                            <div class="form-container pos-relative">
                                <input class="form-control" type="email" name="news-email" placeholder="Enter Your Email">
                                <input class="mt-lg-0 mt-4" type="submit" value="Subscribe">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-1 col-12 full-info mt-lg-0 mt-4">
                    <div class="address h5 f-light lh-35 mb-3">Full Address Here Full Address Here Full Address Here Full Address Here Full Address Here </div><a class="d-inline-block view-map h5 white-text" href="#">View The Map</a>
                    <div class="div mb-4 mt-4 white-bg d-block"></div>
                    <div class="contact"><a class="d-block phone h5 mb-3 white-text" href="tel:01122548033"><span>01122548033</span></a><a class="d-block email h5 mb-3 white-text" href="mailto:info@saphir.com"><span>info@saphir.com</span></a></div>
                    <div class="follow"><a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="#" data-bs-toggle="tooltip" title="facebook"><i class="fab fa-facebook-f"></i></a><a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="#" data-bs-toggle="tooltip" title="twitter"><i class="fab fa-twitter"></i></a><a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="#" data-bs-toggle="tooltip" title="instagram"><i class="fab fa-instagram"></i></a><a class="d-inline-block pos-relative h5 ps-lg-0 pe-lg-4 ps-3 pe-3 me-lg-3 me- ms-2 second-hover pt-3 pb-3 white-text" href="#" data-bs-toggle="tooltip" title="youtube"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
