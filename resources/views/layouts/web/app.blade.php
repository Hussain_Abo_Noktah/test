<!DOCTYPE html>
<html lang="{{app()->getLocale()}}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#00402F">
    <meta name="keywords" content="Saphir">
    <meta name="description" content="Saphir">
    <title>@yield('title')</title>
    <!--for ltr
    -->

    <!--for rtl
    -->
    @if(app()->getLocale() == 'ar')
        <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/bootstrap.rtl.min.css')}}">
   @else
        <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/bootstrap.min.css')}}">
    @endif

    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap"
        rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/all.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/aos.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/animate.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/theme-basic.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/slick.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/slick-theme.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/web/css/main.css')}}">
    <!--for rtl
    -->
    <!--link(type="text/css", rel="stylesheet", href="css/rtl.css")-->

    <link rel="shortcut icon" href="{{asset('css/web/imgs/icon.ico')}}">
</head>
<body>
@include('layouts.web.partial.navbar')

@yield('content')
@include('layouts.web.partial.footer')

</body>
</html>
