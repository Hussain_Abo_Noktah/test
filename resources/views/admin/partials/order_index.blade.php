<x-card-content>
    <x-card-header>
        <x-card-title>
            {{$title}}
        </x-card-title>
        @if(!isset($is_service_provider))
            <x-card-toolbar>
                <a href="{{route('orders.create', ['user_id' => $user ?? null])}}"
                   class="btn btn-primary font-weight-bolder">
                    <i class="la la-plus"></i>
                    {{__('Create New Order')}}
                </a>
                <a href="{{route('orders.unlisted.create', ['user_id' => $user ?? null])}}"
                   class="btn btn-primary font-weight-bolder mx-3">
                    <i class="la la-plus"></i>
                    {{__('Create Unlisted New Order')}}
                </a>
                <a href="{{route('orders.export', ['status' => request()->status, 'user_id' => $user ?? null])}}"
                   class="btn btn-success font-weight-bolder">
                    <i class="la flaticon2-download"></i>
                    {{__('Download ExcelSheet')}}
                </a>
            </x-card-toolbar>
        @endif
    </x-card-header>
    <x-card-body>
        <x-datatable.html-structure>
            <th>{{__('Date')}}</th>
            <th>{{__('Order Number')}}</th>
            <th>{{__('Order Date')}}</th>
            <th>{{__('Client')}}</th>
            <th>{{__('Details')}}</th>
            <th>{{__('Created at')}}</th>
            <th>{{__('Updated at')}}</th>
            <th>{{__('Status')}}</th>
        </x-datatable.html-structure>
    </x-card-body>
</x-card-content>
<x-model.delete-model-confirmation/>
@section('scripts')
    <x-datatable.script
        :columns="$columns"
        :route="route('orders.datatables', ['status' => request()->status,'user_id' => $user ?? null])"
        :enableId="true"
    />
@endsection
