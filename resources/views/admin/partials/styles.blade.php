@if(app()->getLocale() == "en")
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,300;0,400;1,300;1,400&display=swap" rel="stylesheet">
@else
    <style>
        @font-face {
            src: url("{{asset('fonts/Droid.Arabic.ttf')}}");
            font-family: DroidArabic;
        }

        .bootstrap-select .dropdown-toggle .filter-option {
            text-align: right !important;
        }

        div.dataTables_wrapper div.dataTables_filter {
            text-align: left !important;
        }
    </style>
@endif
<link rel="stylesheet" type="text/css" href="{{asset('admin/plugins/global/plugins.bundle.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('admin/plugins/custom/prismjs/prismjs.bundle.css')}}"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">


@if(app()->getLocale() == "en")
    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/style.bundle.css')}}"/>
@else

    <link rel="stylesheet" type="text/css" href="{{asset('admin/css/style.bundle.rtl.css')}}"/>
@endif



<link rel="stylesheet" type="text/css" href="{{asset('admin/css/themes/layout/header/base/'. session()->get('theme_color') . '.' . 'css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/themes/layout/header/menu/'. session()->get('theme_color') . '.'   . 'css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/themes/layout/brand/'. session()->get('theme_color') . '.' . 'css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/themes/layout/aside/'. session()->get('theme_color') . '.' .  'css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('admin/plugins/custom/datatables/datatables.bundle.' . 'css')}}"/>
<link href="{{asset('admin/css/intlTelInput.min.css')}}" rel="stylesheet" type="text/css">
<style>
    sup {
        color: red !important;
        font-size: 18px;
    }

    .iti {
        display: block !important;
    }

    @if(LaravelLocalization::getCurrentLocaleDirection() == "rtl")
    .iti__country-list {
        text-align: right !important;
    }

    .iti--separate-dial-code .iti__selected-dial-code {
        margin-right: 6px;
    }
    @endif
</style>
