<script>
    let KTAppSettings = {
        "breakpoints": {
            "sm": 576,
            "md": 768,
            "lg": 992,
            "xl": 1200,
            "xxl": 1400
        },
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#3699FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#E4E6EF",
                    "dark": "#181C32"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1F0FF",
                    "secondary": "#EBEDF3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "inverse": {
                    "white": "#ffffff",
                    "primary": "#ffffff",
                    "secondary": "#3F4254",
                    "success": "#ffffff",
                    "info": "#ffffff",
                    "warning": "#ffffff",
                    "danger": "#ffffff",
                    "light": "#464E5F",
                    "dark": "#ffffff"
                }
            },
            "gray": {
                "gray-100": "#F3F6F9",
                "gray-200": "#EBEDF3",
                "gray-300": "#E4E6EF",
                "gray-400": "#D1D3E0",
                "gray-500": "#B5B5C3",
                "gray-600": "#7E8299",
                "gray-700": "#5E6278",
                "gray-800": "#3F4254",
                "gray-900": "#181C32"
            }
        },
        "font-family": 'Tajawal", Poppins, Helvetica, "sans-serif"'
    };
</script>
<script src="{{asset('admin/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('admin/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
<script src="{{asset('admin/js/scripts.bundle.js')}}"></script>
<script src="{{asset('admin/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@auth()
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://www.gstatic.com/firebasejs/8.6.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.6.1/firebase-analytics.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.6.1/firebase-messaging.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script type="text/javascript">
    </script>
    @production
        <script type="text/javascript">
            const messaging = firebase.messaging();
            if ('serviceWorker' in navigator) {
                toastr.options.escapeHtml = false;
                if ('serviceWorker' in navigator) {
                    navigator.serviceWorker.register("{{route('settings.init.firebase')}}").then((registration) => {
                        messaging.useServiceWorker(registration);
                        messaging.requestPermission()
                            .then(function () {
                                console.log('Notification permission granted.');
                                getRegToken();
                            })
                            .catch(function (err) {
                                console.log('Unable to get permission to notify.', err);
                            });

                    });
                }


                function getRegToken(argument) {
                    messaging.getToken().then(function (currentToken) {
                        if (currentToken) {
                            saveToken(currentToken);
                            console.log(currentToken);
                        } else {
                            console.log('No Instance ID token available. Request permission to generate one.');
                        }
                    }).catch(function (err) {
                        console.log('An error occurred while retrieving token. ', err);
                    });
                }

                function saveToken(currentToken) {
                    $.ajax({
                        type: "POST",
                        data: {
                            'device_token': currentToken,
                            'api_token': '{!! auth()->user()->api_token !!}',
                            '_method': "PUT",
                            "_token": "{{csrf_token()}}"
                        },
                        url: '{!! route('api.users.update', auth()->user()->id) !!}',
                        success: function (data) {
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }

                function updateGeneralSetting(e) {
                    $.ajax({
                        url: "{{route('settings.general-setting.update')}}",
                        method: 'POST',
                        data: {
                            _method: 'PUT',
                            name: $(e).data('name'),
                            _token: '{{csrf_token()}}'
                        },
                        beforeSend: function () {
                            $(this).attr('disabled', true);
                        },
                        success: function (result) {
                            $(this).attr('disabled', false);
                            toastr.success(result.message);
                        }
                    });
                }
            }
        </script>
    @endproduction
@endauth
<script>
    $('.select2').select2({
        width: "100%"
    });

    function changeStatus(e) {
        $.ajax({
            url: $(e).data('href'),
            method: 'POST',
            data: {
                _method: 'PUT',
                status: $(e).val(),
                _token: '{{csrf_token()}}'
            },
            beforeSend: function () {
                $('#overlay').addClass('overlay overlay-block')
                let html = '<div class="overlay-layer bg-dark-o-10"><div class="spinner spinner-primary"></div></div>';
                $('#data').html(html);
            },
            success: function (result) {
                $('#overlay').removeClass('overlay overlay-block');
                $('#data').html('');
                toastr.success(result.message);
            }
        });
    }

    $(function () {
        let table = $('#datatables').DataTable({
            ordering: true,
            processing: true,
            serverSide: true,
            responsive: true,
            searchDelay: 500,
            @if(app()->getLocale() == 'ar')
            language: {
                "url": "{{asset('admin/datatables/json/arabic.json')}}"
            },
            @endif
        });
        $('.summernote').summernote({
            height: 200
        });
        $('.selectpicker').selectpicker();
        var arrows;

        if (KTUtil.isRTL()) {
            arrows = {
                leftArrow: '<i class="la la-angle-right"></i>',
                rightArrow: '<i class="la la-angle-left"></i>'
            };
        } else {
            arrows = {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            };
        }
        $('.kt-select-date').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });
        $(function () {
            $('#delete').on('show.bs.modal', function (e) {
                $(this).find('.delete_form').attr('action', $(e.relatedTarget).data('href'));
            });
            $('#delete .delete_form').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var action = form.attr('action');
                $.ajax({
                    url: action,
                    type: 'POST',
                    data: form.serialize(),
                    beforeSend: function beforeSend() {
                        $('#body_overlay').addClass('overlay overlay-block');
                        var html = '<div class="overlay-layer bg-dark-o-10"><div class="spinner spinner-primary"></div></div>';
                        $('#spinner').html(html);
                    },
                    success: function success(result) {
                        table.ajax.reload();
                        $('#body_overlay').removeClass('overlay overlay-block');
                        $('#spinner').html('');
                        toastr.success(result.message);
                        $('#delete').modal('hide');
                    },
                    error: function error(response) {
                        var errors = response.responseJSON.errors;
                        '#body_overlay'.removeClass('overlay overlay-block');
                        $('#spinner').html('');
                        $('#delete').modal('hide');
                        $.each(errors, function (key, value) {
                            toastr.error(value);
                        });
                    }
                });
            });
        });
    });
</script>
