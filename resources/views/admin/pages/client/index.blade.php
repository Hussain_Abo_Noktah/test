@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('Clients')}}
            </x-card-title>
            <x-card-toolbar>

            </x-card-toolbar>
        </x-card-header>
        <x-card-body>
            <x-datatable.html-structure>
                <th>{{__('First Name')}}</th>
                <th>{{__('Last Name')}}</th>
                <th>{{__('Email')}}</th>
                <th>{{__('Phone')}}</th>
                <th>{{__('Age')}}</th>
                <th>{{__('Created at')}}</th>
                <th>{{__('status')}}</th>

            </x-datatable.html-structure>
        </x-card-body>
    </x-card-content>
    <x-model.delete-model-confirmation/>
@endsection

@section('scripts')
    <x-datatable.script
        :columns="$columns"
        :route="route('clients.datatables')"
        :enableId="true"
    />
@endsection
