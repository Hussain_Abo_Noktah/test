@extends('admin.layouts.app')
@section('content')
    <form id="create" action="{{route('clients.update',$client->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Edit Client')}}
                </x-card-title>
                <x-card-toolbar>
                    <x-back-btn :route="route('clients.index')"/>
                </x-card-toolbar>
            </x-card-header>
            <x-card-body>

                <div class="row">
                    <x-input-field :title="__('First Name')" name="first_name" type="text" required col="4"
                                   :model="$client"/>
                    <x-input-field :title="__('Family Name')" name="family_name" type="text" required col="4"
                                   :model="$client"/>
                    <x-input-field :title="__('Mobile')" name="mobile" type="text" required col="4"
                                   :model="$client"/>
                    <x-input-field :title="__('Email')" name="email" type="text" required col="4"
                                   :model="$client"/>

                    <x-input-field :title="__('Age')" name="age" type="text" required col="4" :model="$client"/>

                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.update-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop
