@extends('admin.layouts.app')

@section('content')

    <div class="row mt-4">
        <div class="col-md-12 mt-5">
            <x-card-content>
                <x-card-header>
                    <x-card-title>
                        {{__('Client ')}}
                    </x-card-title>
                </x-card-header>
                <x-card-body>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>{{__('First Name')}}</td>
                            <td>{{$client->first_name }}</td>
                        </tr>
                        <tr>
                            <td>{{__('Family Name')}}</td>
                            <td>{{$client->family_name}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Mobile')}}</td>
                            <td>{{$client->mobile}}</td>
                        </tr>

                        <tr>
                            <td>{{__('Email')}}</td>
                            <td>{{$client->email}}</td>
                        </tr>
                        <tr>
                            <td>{{__('age')}}</td>
                            <td>{{$client->age}}</td>
                        </tr>

                    </table>
                </x-card-body>
            </x-card-content>
        </div>
        <div class="col-md-12 mt-5">
            <form id="create" action="{{route('clients.main-details.update',['id' => $client->id])}}" method="post"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <x-card-content>
                    <x-card-header>
                        <x-card-title>
                            {{__('Main Details')}}
                        </x-card-title>
                    </x-card-header>
                    <x-card-body>

                        <div class="row">
                            <x-input-field :title="__('Country')" name="country" type="text" required col="3"
                                           :model="$client->client_details"/>

                            <x-input-field :title="__('City')" name="city" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Address')" name="address" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Street')" name="street" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Home number')" name="house_number" type="text" required col="3"
                                           :model="$client->client_details"/>

                            <x-input-field :title="__('Zip Code')" name="zip_code" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Born Place')" name="born_place" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <div class="col-md-3 form-group">
                                <label for="date">{{__('Birthday')}} <sup>*</sup></label>
                                <input class="date form-control" name="birthday"
                                       value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                       type="text"
                                       id="date"
                                       required placeholder="{{__('Birthday')}}">
                            </div>


                        </div>
                        <x-buttons.update-button/>

                    </x-card-body>

                </x-card-content>
            </form>
        </div>

        <div class="col-md-12 mt-5">
            <form id="create" action="{{route('clients.main-details.update',['id' => $client->id])}}" method="post"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <x-card-content>
                    <x-card-header>
                        <x-card-title>
                            {{__('Education')}}
                        </x-card-title>
                    </x-card-header>
                    <x-card-body>

                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group A') . ' , ' . __('PHD Degree') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">
                                    <x-input-field :title="__('Subject')" name="phd_subject" type="text" required
                                                   col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('Topic')" name="phd_topic" type="text" required col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('University')" name="phd_university" type="text" required
                                                   col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country University')" name="phd_country_university"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>

                                    <x-input-field :title="__('City University')" name="phd_city_university" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="phd_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('Birthday')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group B') .' , '. __('Masters Degree') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">
                                    <x-input-field :title="__('Subject')" name="master_subject" type="text" required
                                                   col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('Topic')" name="master_topic" type="text" required col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('University')" name="master_university" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country University')" name="master_country_university"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>

                                    <x-input-field :title="__('City University')" name="master_city_university"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('From Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="master_from_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('From Year')}}">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('To Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="master_from_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('To Year')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group C') .' , '. __(' Bachelor  Degree') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">

                                    <x-input-field :title="__('Department')" name="bachelor_department" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('University')" name="bachelor_university" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country University')" name="bachelor_country_university"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>

                                    <x-input-field :title="__('City University')" name="bachelor_city_university"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('From Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="bachelor_from_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('From Year')}}">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('To Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="bachelor_from_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('To Year')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group D') .' , '. __('Abitur') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">


                                    <x-input-field :title="__('High School')" name="abitur_high_school" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country High School')" name="abitur_country_high_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>

                                    <x-input-field :title="__('City High School')" name="abitur_city_high_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('Graduate Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="abitur_graduate_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('Graduate Year')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group E') .' , '. __('Fachabitur') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">


                                    <x-input-field :title="__('High School')" name="fachabitur_high_school" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country High School')"
                                                   name="fachabitur_country_high_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>

                                    <x-input-field :title="__('City High School')" name="fachabitur_city_high_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('Graduate Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="abitur_graduate_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('Graduate Year')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group F') .' , '. __('Vocational School ') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">


                                    <x-input-field :title="__('Training Name')" name="training_name" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>

                                    <x-input-field :title="__('School Attend')" name="training_school_attend"
                                                   type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Training Degree')" name="training_degree"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country  School Training')"
                                                   name="training_country_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('City High School')" name="training_city_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('Training Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="training_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('Training Year')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group G') .' , '. __('Secondary School') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">


                                    <x-input-field :title="__('School Name')" name="school_name" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country Secondary School')"
                                                   name="secondery_country_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('City Secondary School')"
                                                   name="secondary_city_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('Secondary Graduate Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="secondary_graduate_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('Secondary Graduate Year')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Group H') .' , '. __('Middle School') }}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">


                                    <x-input-field :title="__('School Name')" name="middle_school_name" type="text"
                                                   required col="3"
                                                   :model="$client->client_details"/>


                                    <x-input-field :title="__('Country Secondary School')"
                                                   name="middle_country_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('City Secondary School')"
                                                   name="middle_city_school"
                                                   type="text" required col="3"
                                                   :model="$client->client_details"/>


                                    <div class="col-md-3 form-group">
                                        <label for="date">{{__('Graduate Year')}} <sup>*</sup></label>
                                        <input class="date form-control" name="middle_graduate_year"
                                               value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                               type="text"
                                               id="date"
                                               required placeholder="{{__('Secondary Graduate Year')}}">
                                    </div>


                                </div>


                            </x-card-body>

                        </x-card-content>
                        <x-card-content>
                            <x-card-header>
                                <x-card-title>
                                    {{__('Addition  Educations')}}
                                </x-card-title>
                            </x-card-header>
                            <x-card-body>

                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="category"> Language Course Name <sup>*</sup></label>
                                        <select class="select2 form-control" name="addition_edu_course_name" required
                                                id="category">
                                            <option value="" selected disabled>{{__('Choose Course Name')}}</option>

                                            <option value="integration_course">
                                                {{__('Integration Course')}}
                                            </option>
                                            <option value="literacy_course">
                                                {{__('Literacy course with attached integration course')}}
                                            </option>
                                            <option value="vocational_course">
                                                {{__('Vocational language course with target level A1, A2, B1, B2, C1')}}
                                            </option>
                                            <option value="dsh_course">

                                                {{__('DSH course with target level B2, C1, C2')}}
                                            </option>
                                            <option value="test_daf_course">

                                                {{__('Test DaF Course')}}
                                            </option>
                                            <option value="german_language_course">

                                                {{__('German language course with target level A1, A2, B1, B2, C1')}}
                                            </option>

                                        </select>
                                    </div>
                                    <x-input-field :title="__('School')" name="school" type="text" required col="3"
                                                   :model="$client->client_details"/>
                                    <x-input-field :title="__('Country')" name="country" type="text" required col="3"
                                                   :model="$client->client_details"/>

                                    <x-input-field :title="__('City School Located')" name="city_school_located" type="text" required col="3"
                                                   :model="$client->client_details"/>



                                </div>


                            </x-card-body>

                        </x-card-content>

                        <x-buttons.update-button/>
                    </x-card-body>

                </x-card-content>
            </form>
        </div>

        <div class="col-md-12 mt-5">
            <form id="create" action="{{route('clients.main-details.update',['id' => $client->id])}}" method="post"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <x-card-content>
                    <x-card-header>
                        <x-card-title>
                            {{__('Internship')}}
                        </x-card-title>
                    </x-card-header>
                    <x-card-body>

                        <div class="row">
                            <x-input-field :title="__('Internship name')" name="internship_name" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Department')" name="area" type="text" required col="3"
                                           :model="$client->client_details"/>

                            <x-input-field :title="__('Company')" name="department" type="text" required col="3"
                                           :model="$client->client_details"/>


                            <x-input-field :title="__('Area')" name="department" type="text" required col="3"
                                           :model="$client->client_details"/>

                            <x-input-field :title="__('City')" name="city" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Address')" name="address" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Street')" name="street" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Home number')" name="house_number" type="text" required col="3"
                                           :model="$client->client_details"/>

                            <x-input-field :title="__('Zip Code')" name="zip_code" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <x-input-field :title="__('Born Place')" name="born_place" type="text" required col="3"
                                           :model="$client->client_details"/>
                            <div class="col-md-3 form-group">
                                <label for="date">{{__('Date')}} <sup>*</sup></label>
                                <input class="date form-control" name="birthday"
                                       value="@if($client->client_details  != null){{ $client->client_details->birthday}}  @else {{old('date')}} @endif"
                                       type="text"
                                       id="date"
                                       required placeholder="{{__('Birthday')}}">
                            </div>


                        </div>
                        <x-buttons.update-button/>

                    </x-card-body>

                </x-card-content>
            </form>
        </div>
    </div>

@stop

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>


    <script>
        flatpickr('.date', {
            enableTime: false
        })
    </script>
    <script>


        let incrementor = 0;
        $("#addTask").on('click', function () {
            let html = '<div class="row" id="task_id_' + incrementor + '">';
            html += '<div class="col-md-10">';
            html += "<div class='form-group'>";
            html += '<label for="task_name">{{__('Task Name')}} <sup>*</label>';
            html += '<input name="task_name[]" class="form-control" required placeholder="{{__('Task Name')}}" required/>';
            html += "</div>";
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<button type="button" style="margin-top: 26px;" data-taskid="' + incrementor + '" class="btn btn-danger removeTask btn-block d-block">{{__("Remove")}}</button>';
            html += '</div>';

            html += '</div>';
            $('#tasks').addClass("mt-5");
            $('#tasks').append(html);
            incrementor++;
        });
        $(document).on('click', '.removeTask', function () {
            $('#task_id_' + $(this).data('taskid')).remove();
        })
    </script>
@endsection
