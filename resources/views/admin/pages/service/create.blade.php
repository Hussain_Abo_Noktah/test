@extends('admin.layouts.app')

@section('content')
    <form id="create" action="{{route('services.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Add New Service')}}
                </x-card-title>
                <x-card-toolbar>
                    <x-back-btn :route="route('services.index')"/>
                </x-card-toolbar>
            </x-card-header>
            <x-card-body>
                <div class="row">
                    <x-input-field :title="__('Name In Arabic')" name="name_ar" type="text" required col="4"/>
                    <x-input-field :title="__('Name In English')" name="name_en" type="text" required col="4"/>
                    <x-input-field :title="__('Name In Deutch')" name="name_de" type="text" required col="4"/>
                    <x-input-field :title="__('Cost')" name="cost" type="text" required col="4"/>

                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In English') }}</label>
                            <textarea name="description_en" id="summernote" class=" summernote form-control round">{{old('description_en')}}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In Arabic') }}</label>
                            <textarea name="description_ar" id="summernote" class="summernote form-control round">{{old('description_ar')}}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In Deutch') }}</label>
                            <textarea name="description_de" id="summernote" class="summernote form-control round">{{old('description_de')}}</textarea>
                        </fieldset>
                    </div>
                </div>
                <div class="col-md-8">
                    <x-fields.file-field :title="__('Image')" name="image" :collection="\App\Models\Service::$SERVICE_IMAGE" />
                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.save-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop



