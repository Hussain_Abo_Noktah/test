@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('Services')}}
            </x-card-title>
            <x-card-toolbar>
         <a href="{{route('services.create')}}" class="btn btn-primary font-weight-bolder">
                    <i class="la la-plus"></i>
                    {{__('Create New Service')}}
                </a>
            </x-card-toolbar>
        </x-card-header>
        <x-card-body>
            <x-datatable.html-structure>
                <th>{{__('Image')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Description')}}</th>
                <th>{{__('Cost')}}</th>
                <th>{{__('Updated at')}}</th>
                <th>{{__('status')}}</th>

            </x-datatable.html-structure>
        </x-card-body>
    </x-card-content>
    <x-model.delete-model-confirmation/>
@endsection

@section('scripts')
    <x-datatable.script
        :columns="$columns"
        :route="route('services.datatables')"
        :enableId="true"
    />
@endsection
