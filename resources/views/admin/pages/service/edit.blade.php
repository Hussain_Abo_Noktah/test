@extends('admin.layouts.app')
@section('content')
<form id="create" action="{{route('services.update',$service->id)}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{ __('Edit Page')}}
            </x-card-title>
            <x-card-toolbar>
                <x-back-btn :route="route('services.index')"/>
            </x-card-toolbar>
        </x-card-header>
        <x-card-body>
            <div class="row">
                <x-input-field :title="__('Name In Arabic')" name="name_ar" type="text" required col="4"
                               :model="$service"/>
                <x-input-field :title="__('Name In English')" name="name_en" type="text" required col="4"
                               :model="$service"/>
                <x-input-field :title="__('Name In Deutch')" name="name_de" type="text" required col="4"
                               :model="$service"/>

                <x-input-field :title="__('Cost')" name="cost" type="text" required col="4"  :model="$service"/>

                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="roundText">{{ __('Description In English') }}</label>
                        <textarea name="description_en" id="summernote" class=" summernote form-control round"  >{{$service->description_en}}</textarea>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="roundText">{{ __('Description In Arabic') }}</label>
                        <textarea name="description_ar" id="summernote" class="summernote form-control round">{{$service->description_ar}}</textarea>
                    </fieldset>
                </div>
                <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="roundText">{{ __('Description In Deutch') }}</label>
                        <textarea name="description_ku" id="summernote" class="summernote form-control round">{{$service->description_de}}</textarea>
                    </fieldset>
                </div>

                <div class="col-md-8">
                    <x-fields.file-field :title="__('Image')" name="image"
                                         :collection="\App\Models\Service::$SERVICE_IMAGE" :model="$service"/>
                </div>
            </div>
        </x-card-body>
        <x-card-footer>
            <x-buttons.update-button/>
        </x-card-footer>
    </x-card-content>
</form>
@stop
