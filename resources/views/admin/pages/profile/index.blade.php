@extends('admin.layouts.app')

@section('content')
    <form action="{{route('profile.update',auth()->user()->id)}}" method="post">
        @csrf
        @method('put')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    <h3>{{__('School Profile')}}</h3>
                </x-card-title>
            </x-card-header>
            <x-card-body>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">{{__('Name')}} <sup>*</sup></label>
                            <input type="text" name="name" class="form-control"
                                   value="{{old('name', auth()->user()->name)}}" id="name" required placeholder="Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="username">{{__('Username')}} <sup>*</sup></label>
                            <input type="text" name="username" class="form-control"
                                   value="{{old('username', auth()->user()->username)}}" id="username" required
                                   placeholder="Username">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">{{__('Email')}} <sup>*</sup></label>
                            <input type="email" name="email" class="form-control"
                                   value="{{old('email', auth()->user()->email)}}" id="email" required
                                   placeholder="Email">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_phone">{{__('Phone Number')}} <sup>*</sup></label>
                            <input placeholder="{{__('Your phone number')}}" id="first_phone" type="text"
                                   class="form-control @error('first_phone') is-invalid @enderror" name="first_phone"
                                   value="{{ old('first_phone', auth()->user()->first_phone) }}" required
                                   autocomplete="phone" autofocus>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label for="password">{{__('Password')}}</label>
                        <input type="password" name="password" class="form-control" id="password"
                               placeholder="Password">
                    </div>
                    <div class="col-md-6">
                        <label for="password_confirmation">{{__('Password Confirmation')}}</label>
                        <input type="password" name="password_confirmation" class="form-control"
                               id="password_confirmation" placeholder="Password Confirmation">
                    </div>
                </div>
            </x-card-body>
            <x-card-footer>
                <button class="btn btn-success" type="submit">
                    {{__('Update Profile')}}
                </button>
            </x-card-footer>
        </x-card-content>
    </form>
@endsection
