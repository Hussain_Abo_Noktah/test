@extends('admin.layouts.app')

@section('content')
    <form action="{{route('setting.mail.update')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Update Setting')}}
                </x-card-title>

            </x-card-header>
            <x-card-body>
                <div class="row">
                    <input type="hidden" value="mailgun" name="mail_driver" class="form-control " required=""
                           placeholder="mail driver" id="mail_driver">
                    <input type="hidden" value="mailgun" name="type">
                        <div class="form-group col-md-4">
                            <label for="mailgun_domain">mailgun_domain <sup>*</sup></label>
                            <input type="text" value="{{$setting['mailgun_domain']}}" name="mailgun_domain" class="form-control "
                                   required="" placeholder="mailgun domain" id="mailgun_domain">
                        </div>


                        <div class="form-group col-md-4">
                            <label for="mailgun_secret">mailgun_secret <sup>*</sup></label>
                            <input type="text" value="{{$setting['mailgun_secret']}}" name="mailgun_secret" class="form-control " required=""
                                   placeholder="mailgun_secret" id="mailgun_secret">
                        </div>
                    </div>




            </x-card-body>
            <x-card-footer>
                <x-buttons.save-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop



