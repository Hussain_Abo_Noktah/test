@extends('admin.layouts.app')

@section('content')
    <form action="{{route('setting.mail.update')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Update Setting')}}
                </x-card-title>

            </x-card-header>
            <x-card-body>
                <div class="row">
                    <input type="hidden" value="postmark" name="mail_driver" class="form-control " required=""
                           placeholder="mail driver" id="mail_driver">
                    <input type="hidden" value="postmark" name="type">
                        <div class="form-group col-md-4">
                            <label for="postmark token">Postmark token <sup>*</sup></label>
                            <input type="text" value="{{$setting['postmark_token']}}" name="postmark_token" class="form-control "
                                   required="" placeholder="sparkpost_secret" id="sparkpost_secret">
                        </div>



                    </div>




            </x-card-body>
            <x-card-footer>
                <x-buttons.save-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop



