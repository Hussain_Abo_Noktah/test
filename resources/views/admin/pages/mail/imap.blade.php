@extends('admin.layouts.app')

@section('content')
    <form  action="{{route('setting.mail.update')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Update Setting')}}
                </x-card-title>

            </x-card-header>
            <x-card-body>
                <div class="row">
                    <input type="hidden" value="imap" name="mail_driver" class="form-control " required=""
                           placeholder="mail driver" id="mail_driver">

                    <input type="hidden" value="imap" name="type">
                    <div class="form-group col-md-4">
                        <label for="mail_host">mail_host <sup>*</sup></label>
                        <input type="text" value="{{$setting['mail_host']}}" name="mail_host" class="form-control "
                               required="" placeholder="mail host" id="mail_host">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="mail_port">mail_port <sup>*</sup></label>
                        <input type="text" value="{{$setting['mail_port']}}" name="mail_port" class="form-control " required=""
                               placeholder="mail port" id="mail_port">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="mail_username">mail_username <sup>*</sup></label>
                        <input type="text" value="{{$setting['mail_username']}}" name="mail_username"
                               class="form-control " required="" placeholder="mail username" id="mail_username">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="mail_password">mail_password <sup>*</sup></label>
                        <input type="text" value="{{$setting['mail_password']}}" name="mail_password" class="form-control " required=""
                               placeholder="mail_password" id="mail_password">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="mail_encryption">Mail encryption <sup>*</sup></label>
                        <input type="text" value="{{$setting['mail_encryption']}}" name="mail_encryption" class="form-control " required=""
                               placeholder="mail_encryption" id="mail_encryption">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="mail_from_address">Mail from address <sup>*</sup></label>
                        <input type="text" value="{{$setting['mail_from_address']}}" name="mail_from_address"
                               class="form-control " required="" placeholder="mail_from_address"
                               id="mail_from_address">

                    </div>
                    <div class="form-group col-md-4">
                        <label for="mail_from_name">Mail from  name <sup>*</sup></label>
                        <input type="text" value="{{$setting['mail_from_name']}}" name="mail_from_name" class="form-control "
                               required="" placeholder="mail from name" id="mail_from_name">

                    </div>

                </div>


            </x-card-body>
            <x-card-footer>
                <x-buttons.save-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop



