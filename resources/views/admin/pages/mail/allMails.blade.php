@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('E-Mails')}}
            </x-card-title>
            <x-card-toolbar>


            </x-card-toolbar>
        </x-card-header>
        <x-card-body>

            <table class="table">
                <thead>
                <tr>
                    <th scope="col">UID</th>
                    <th scope="col">Subject</th>
                    <th scope="col">From</th>
                    <th scope="col">To</th>
                    <th scope="col">Attachments</th>
                    <th scope="col">Date</th>
                </tr>
                </thead>
                <tbody>
                <?php if($paginator->count() > 0): ?>
                <?php foreach($paginator as $message): ?>
                <tr>
                    <td><?php echo $message->getUid(); ?></td>
                    <td><?php echo $message->getSubject(); ?></td>
                    <td><?php echo $message->getFrom()[0]->mail; ?></td>
                    <td><?php echo $message->getTo()[0]->mail; ?></td>
                    <td><?php echo $message->getAttachments()->count() > 0 ? 'yes' : 'no'; ?></td>
                    <td><?php echo $message->getDate(); ?></td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td colspan="4">No messages found</td>
                </tr>
                <?php endif; ?>
                </tbody>
            </table>

            <?php echo $paginator->links(); ?>
        </x-card-body>
    </x-card-content>

@endsection


