@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('E-Mails')}}
            </x-card-title>
            <x-card-toolbar>
                <a href="{{route('mails.create')}}" class="btn btn-primary font-weight-bolder">
                    <i class="la la-plus"></i>
                    {{__('Create New Message')}}
                </a>

            </x-card-toolbar>
        </x-card-header>
        <x-card-body>
            <x-datatable.html-structure>
                <th>{{__('From')}}</th>
                <th>{{__('Name')}}</th>
                <th>{{__('Title')}}</th>
                <th>{{__('Subject')}}</th>
                <th>{{__('Body')}}</th>
                <th>{{__('Date')}}</th>


            </x-datatable.html-structure>
        </x-card-body>
    </x-card-content>
    <x-model.delete-model-confirmation/>
@endsection

@section('scripts')
    <x-datatable.script
        :columns="$columns"
        :route="route('mails.datatables' , ['type'=> \App\Models\Mail::$INBOX])"
        :enableId="true"
    />
@endsection
