@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{ app()->isLocale('ar') ? $page->title_ar : $page->title_en}}
            </x-card-title>
            <x-card-toolbar>
                <x-back-btn :route="route('pages.index', ['status' => \App\Models\Order::$ALL])"/>
            </x-card-toolbar>
        </x-card-header>
    </x-card-content>

    <div class="row mt-12">
        <div class="col-md-12">
            <x-card-content>
                <x-card-header>
                    <x-card-title>
                        {{__('Page Details')}}
                    </x-card-title>
                </x-card-header>
                <x-card-body>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>{{__('Title In English')}}</td>
                            <td>{{$page->title_en}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Title In Arabic')}}</td>
                            <td>{{$page->title_ar}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Description In English')}}</td>
                            <td>{{$page->description_en}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Description In Arabic')}}</td>
                            <td>{{$page->description_ar}}</td>
                        </tr>
                    </table>
                </x-card-body>
            </x-card-content>
        </div>
    </div>

@endsection
