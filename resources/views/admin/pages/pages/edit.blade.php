@extends('admin.layouts.app')
@section('content')
    <form id="create" action="{{route('pages.update',$page->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Edit Page')}}
                </x-card-title>
                <x-card-toolbar>
                    <x-back-btn :route="route('pages.index')"/>
                </x-card-toolbar>
            </x-card-header>
            <x-card-body>
                <div class="row">
                 <x-input-field :title="__('Title In English')" name="title_en" type="text" required col="4" :model="$page"/>
                    <x-input-field :title="__('Title In Arabic')" name="title_ar" type="text" required col="4" :model="$page"/>
                    <x-input-field :title="__('Title In kurdish')" name="title_ku" type="text" required col="4" :model="$page"/>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In English') }}</label>
                            <textarea name="description_en" id="summernote" class=" summernote form-control round">{{ $page->description_en }}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In Arabic') }}</label>
                            <textarea name="description_ar" id="summernote" class="summernote form-control round">{{ $page->description_ar }}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In kurdish') }}</label>
                            <textarea name="description_ku" id="summernote" class="summernote form-control round">{{ $page->description_ku }}</textarea>
                        </fieldset>
                    </div>
                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.update-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop
