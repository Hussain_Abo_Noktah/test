@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-body>
            <div class="row">
                <div class="col-md-4 form-group">
                    <label for="date">{{__('From')}} <sup>*</sup></label>
                    <input class="date form-control" name="from" value="{{request('from')}}" type="text" id="from"
                           required placeholder="{{__('Date')}}">
                </div>
                <div class="col-md-4 form-group">
                    <label for="date">{{__('To')}} <sup>*</sup></label>
                    <input class="date form-control" name="to" value="{{request('to')}}" type="text" id="to"
                           required placeholder="{{__('Date')}}">
                </div>


                <x-fields.select-field
                    :title="__('Service Provider')"
                    name="user_id"
                    :selectBoxData="$vendors"
                    value="id"
                    value-data="name"
                    second-name="id"
                    :model="$user"
                    all="all"
                    required
                />


            </div>
        </x-card-body>
        <x-card-footer>
            <button class="btn btn-primary" id="filters"> إرسال</button>
        </x-card-footer>
    </x-card-content>
    @if($data != null)
    <x-card-content class="mt-5">
        <x-card-header>
            <x-card-title>{{__('result')}}</x-card-title>
        </x-card-header>
        <x-card-content>
            <x-card-body>

                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="card-body" style="background-color:#2490FF ">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                     height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"></rect>
                        <path
                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                            fill="#000000" opacity="0.3"></path>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
                            <div class="text-inverse-primary font-weight-bolder font-size-h3 mb-2 mt-5">{{ __('Completed Orders') }}</div>
                            <div class="font-weight-bold text-inverse-primary font-size-md">{{ $data['count'] }}</div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="card-body" style="background-color:#2490FF ">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                     height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"></rect>
                        <path
                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                            fill="#000000" opacity="0.3"></path>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
                            <div class="text-inverse-primary font-weight-bolder font-size-h3 mb-2 mt-5">{{ __('Total price') }}</div>
                            <div class="font-weight-bold text-inverse-primary font-size-md">{{ $data['total_price'] }}</div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="card-body" style="background-color:#2490FF ">
            <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                     height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"></rect>
                        <path
                            d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                            fill="#000000" opacity="0.3"></path>
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
                            <div class="text-inverse-primary font-weight-bolder font-size-h3 mb-2 mt-5">{{ __('avg') }}</div>
                            <div class="font-weight-bold text-inverse-primary font-size-md">{{ $data['avg'] }}</div>
                        </div>
                    </div>
                </div>
            </x-card-body>
        </x-card-content>
        @endif
    </x-card-content>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr('.date', {
            enableTime: false
        });

    </script>


    <script>

        $(document).ready(function () {
            $("#filters").click(function () {
                $from = $("input[name='from']").val();
                $to = $("input[name='to']").val()
                $provider_id = $("#user_id").val();

                $url = "{{route('statistics')}}" + "?from=" + $from + "&to=" + $to
                if ($provider_id !== null && $provider_id !== 'all') {
                    $url += "&provider=" + $provider_id
                }
                if ($from !== '' && $to !== '') {
                    $(location).attr('href', $url);
                }
                else{
                     alert('أدخل تاريخ البداية والنهاية')
                }
            })
        })
    </script>
@endsection
