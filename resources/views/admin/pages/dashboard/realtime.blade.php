@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('Real Time Orders')}}
            </x-card-title>
        </x-card-header>
    </x-card-content>
    <div class="row mt-5" id="realtime_data">
        @foreach ($orders as $order)
            <div class="col-xl-4">
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-body pt-4 d-flex flex-column justify-content-between">
                        <div class="d-flex align-items-center mb-7">
                            <div class="flex-shrink-0 mr-4 mt-lg-0 mt-3">
                                <div class="symbol symbol-lg-75">
                                    <img alt="Pic" src="{{optional($order->client)->profile_image}}">
                                </div>
                            </div>
                            <div class="d-flex flex-column">
                                <a href="#" class="text-dark font-weight-bold text-hover-primary font-size-h4 mb-0">{{optional($order->client)->name}}</a>
                            </div>
                        </div>
                        <div class="mb-7">
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="text-dark-75 font-weight-bolder mr-2">{{__('Category')}}</span>
                                <a href="#" class="text-muted text-hover-primary">{{optional($order->category)->name ?? "--"}}</a>
                            </div>
                            <div class="d-flex justify-content-between align-items-cente my-1">
                                <span class="text-dark-75 font-weight-bolder mr-2">{{__('Phone:')}}</span>
                                <a href="#" class="text-muted text-hover-primary">{{optional($order->client)->phone}}</a>
                            </div>
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="text-dark-75 font-weight-bolder mr-2">{{__('Order Code')}}</span>
                                <span class="text-muted font-weight-bold">{{$order->order_number}}</span>
                            </div>
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="text-dark-75 font-weight-bolder mr-2">{{__('Created at')}}</span>
                                <span class="text-muted font-weight-bold">{{$order->created_at->diffForHumans()}}</span>
                            </div>
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="text-dark-75 font-weight-bolder mr-2">{{__('Address')}}</span>
                                <span class="text-muted font-weight-bold">{{optional($order->address)->address}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
