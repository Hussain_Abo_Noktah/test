@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('Sections')}}
            </x-card-title>
            <x-card-toolbar>
              <!--  <a href="{{route('sections.create')}}" class="btn btn-primary font-weight-bolder">
                    <i class="la la-plus"></i>
                    {{__('Create New Page')}}
                </a> -->
            </x-card-toolbar>
        </x-card-header>
        <x-card-body>
            <x-datatable.html-structure>
                <th>{{__('Title')}}</th>
                <th>{{__('Body')}}</th>
                <th>{{__('Type')}}</th>

            </x-datatable.html-structure>
        </x-card-body>
    </x-card-content>
    <x-model.delete-model-confirmation/>
@endsection

@section('scripts')
    <x-datatable.script
        :columns="$columns"
        :route="route('sections.datatables', ['status' => 'all'])"
        :enableId="true"
    />
@endsection
