@extends('admin.layouts.app')
@section('content')
    <form id="create" action="{{route('sections.update',$section->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Edit Page')}}
                </x-card-title>
                <x-card-toolbar>
                    <x-back-btn :route="route('sections.index')"/>
                </x-card-toolbar>
            </x-card-header>
            <x-card-body>
                <div class="row">
                    <x-input-field :title="__('Title In English')" name="title_en" type="text" required col="4" :model="$section"/>
                    <x-input-field :title="__('Title In Arabic')" name="title_ar" type="text" required col="4" :model="$section"/>
                    <x-input-field :title="__('Title In kurdish')" name="title_de" type="text" required col="4" :model="$section"/>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Body In English') }}</label>
                            <textarea name="body_en" id="summernote" class=" summernote form-control round">{{ $section->body_en }}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Body In Arabic') }}</label>
                            <textarea name="body_ar" id="summernote" class="summernote form-control round">{{ $section->body_ar }}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Body In kurdish') }}</label>
                            <textarea name="body_de" id="summernote" class="summernote form-control round">{{ $section->body_de }}</textarea>
                        </fieldset>
                    </div>

                    <div class="col-md-8">
                        <x-fields.file-field :title="__('Profile Image')" name="image" :collection="\App\Models\Section::$SECTION_IMAGE" :model="$section"/>
                    </div>
                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.update-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop
