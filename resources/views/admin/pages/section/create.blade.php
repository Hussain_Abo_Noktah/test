@extends('admin.layouts.app')

@section('content')
    <form id="create" action="{{route('sections.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Add New Slider Page')}}
                </x-card-title>
                <x-card-toolbar>
                    <x-back-btn :route="route('sections.index')"/>
                </x-card-toolbar>
            </x-card-header>
            <x-card-body>
                <div class="row">
                    <x-input-field :title="__('Title In Arabic')" name="title_ar" type="text" required col="4"/>
                    <x-input-field :title="__('Title In English')" name="title_en" type="text" required col="4"/>
                    <x-input-field :title="__('Title In Deutch')" name="title_ku" type="text" required col="4"/>

                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In English') }}</label>
                            <textarea name="description_en" id="summernote" class=" summernote form-control round">{{old('description_en')}}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In Arabic') }}</label>
                            <textarea name="description_ar" id="summernote" class="summernote form-control round">{{old('description_ar')}}</textarea>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="form-group">
                            <label for="roundText">{{ __('Description In kurdish') }}</label>
                            <textarea name="description_ku" id="summernote" class="summernote form-control round">{{old('description_ku')}}</textarea>
                        </fieldset>
                    </div>
                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.save-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop



