@extends('admin.layouts.app')
@section('content')
    <form id="create" action="{{route('request-services.update',$request_service->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Edit Service Request')}}
                </x-card-title>
                <x-card-toolbar>
                    <x-back-btn :route="route('request-services.index')"/>
                </x-card-toolbar>
            </x-card-header>
            <x-card-body>
                <div class="row">
                    <x-input-field :title="__('Name')" name="name" type="text" required col="4"
                                   :model="$request_service"/>
                    <x-input-field :title="__('Family Name')" name="family_name" type="text" required col="4"
                                   :model="$request_service"/>
                    <x-input-field :title="__('Mobile')" name="mobile" type="text" required col="4"
                                   :model="$request_service"/>
                    <x-input-field :title="__('Email')" name="email" type="text" required col="4"
                                   :model="$request_service"/>

                    <x-input-field :title="__('Age')" name="age" type="text" required col="4" :model="$request_service"/>
                    <x-select-field :select-box-data="$services" value="id"
                                    value-data="{{'name_' . LaravelLocalization::getCurrentLocale()}}"
                                    :title="__('Service')" name="service_id" required :model="$request_service"/>
                    <x-select-field :select-box-data="$appointments" value="id"
                                    value-data="{{'appointment_date'}}"
                                    :title="__('Appointment')" name="appointment_id" required
                                    :model="$request_service"/>


                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.update-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop
