@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('Request Service')}}
            </x-card-title>
            <x-card-toolbar>

            </x-card-toolbar>
        </x-card-header>
        <x-card-body>
            <x-datatable.html-structure>
                <th>{{__('Name')}}</th>
                <th>{{__('Email')}}</th>
                <th>{{__('phone')}}</th>
                <th>{{__('Age')}}</th>
                <th>{{__('Appointment')}}</th>
                <th>{{__('Service')}}</th>
                <th>{{__('Created at')}}</th>
                <th>{{__('status')}}</th>

            </x-datatable.html-structure>
        </x-card-body>
    </x-card-content>
    <x-model.delete-model-confirmation/>
@endsection

@section('scripts')
    <x-datatable.script
        :columns="$columns"
        :route="route('request-services.datatables')"
        :enableId="true"
    />
@endsection
