@extends('admin.layouts.app')

@section('content')

    <div class="row mt-4">
        <div class="col-md-12 mt-5">
            <x-card-content>
                <x-card-header>
                    <x-card-title>
                        {{__('Request Details')}}
                    </x-card-title>
                </x-card-header>
                <x-card-body>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <td>{{__('Full Name')}}</td>
                            <td>{{$request_service->name . ' ' , $request_service->family_name}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Mobile')}}</td>
                            <td>{{$request_service->mobile}}</td>
                        </tr>

                        <tr>
                            <td>{{__('Email')}}</td>
                            <td>{{$request_service->email}}</td>
                        </tr>
                        <tr>
                            <td>{{__('age')}}</td>
                            <td>{{$request_service->age}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Service')}}</td>
                            <td>{{$request_service->service->name}}</td>
                        </tr>
                        <tr>
                            <td>{{__('Appointment')}}</td>
                            <td>{{$request_service->appointment->appointment_date}}</td>
                        </tr>

                        @if($request_service->status != \App\Models\RequestService::$PENDING)
                            <tr>
                                <td>{{__('Status')}}</td>
                                <td>{{$request_service->status}}</td>
                            </tr>

                        @endif

                    </table>

                    @if($request_service->status == \App\Models\RequestService::$PENDING)
                        <a href="{{route('request-services.accept_or_reject' ,[ 'id'=> $request_service->id ,'status' =>'approved'])}}"
                           class="btn btn-success font-weight-bold">{{__('Approve')}}</a>
                        <a href="{{route('request-services.accept_or_reject' ,['id'=> $request_service->id, 'status' =>'canceled'])}}"
                           class="btn btn-danger font-weight-bold">{{__('Cancel')}}</a>
                    @endif
                </x-card-body>

            </x-card-content>


        </div>


    </div>

@stop

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>


    <script>
        flatpickr('.date', {
            enableTime: false
        })
    </script>
    <script>


        let incrementor = 0;
        $("#addTask").on('click', function () {
            let html = '<div class="row" id="task_id_' + incrementor + '">';
            html += '<div class="col-md-10">';
            html += "<div class='form-group'>";
            html += '<label for="task_name">{{__('Task Name')}} <sup>*</label>';
            html += '<input name="task_name[]" class="form-control" required placeholder="{{__('Task Name')}}" required/>';
            html += "</div>";
            html += '</div>';

            html += '<div class="col-md-2">';
            html += '<button type="button" style="margin-top: 26px;" data-taskid="' + incrementor + '" class="btn btn-danger removeTask btn-block d-block">{{__("Remove")}}</button>';
            html += '</div>';

            html += '</div>';
            $('#tasks').addClass("mt-5");
            $('#tasks').append(html);
            incrementor++;
        });
        $(document).on('click', '.removeTask', function () {
            $('#task_id_' + $(this).data('taskid')).remove();
        })
    </script>
@endsection
