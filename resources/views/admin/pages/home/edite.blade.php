@extends('admin.layouts.app')
@section('content')
    <form id="create" action="{{route('image.update')}}" method="post" enctype="multipart/form-data">
        @csrf

        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Images')}}
                </x-card-title>

            </x-card-header>
            <x-card-body>
                <div class="row">
                    @foreach($images as $image)
                        <div class="col-md-8">
                            <x-fields.file-field :title="$image->type" name="{{$image->type}}"
                                                 :collection="$image->type" :model="$image"/>
                        </div>
                    @endforeach

                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.update-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop
