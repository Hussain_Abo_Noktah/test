@extends('admin.layouts.app')
@section('content')
    <form id="create" action="{{route('home-sliders.update',$slider->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Edit Image Slider')}}
                </x-card-title>
                <x-card-toolbar>
                    <x-back-btn :route="route('home-sliders.index')"/>
                </x-card-toolbar>
            </x-card-header>
            <x-card-body>
                <div class="row">
                    <div class="col-md-8">
                        <x-fields.file-field :title="__('Main Image')" name="slider_image" :collection="\App\Models\HomeSlider::$SLIDER_IMAGE" />
                    </div>
                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.update-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop
