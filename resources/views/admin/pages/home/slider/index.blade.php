@extends('admin.layouts.app')

@section('content')
    <x-card-content>
        <x-card-header>
            <x-card-title>
                {{__('Home Slider')}}
            </x-card-title>
            <x-card-toolbar>
               <a href="{{route('home-sliders.create')}}" class="btn btn-primary font-weight-bolder">
                    <i class="la la-plus"></i>
                    {{__('Create New Image Slider')}}
                </a>
            </x-card-toolbar>
        </x-card-header>
        <x-card-body>
            <x-datatable.html-structure>
                <th>{{__('Main Image')}}</th>



            </x-datatable.html-structure>
        </x-card-body>
    </x-card-content>
    <x-model.delete-model-confirmation/>
@endsection

@section('scripts')
    <x-datatable.script
        :columns="$columns"
        :route="route('home-slider.datatables')"
        :enableId="true"
    />
@endsection
