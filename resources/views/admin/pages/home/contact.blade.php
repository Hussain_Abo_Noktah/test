@extends('admin.layouts.app')
@section('content')
    <form id="create" action="{{route('contact.update',$contact->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <x-card-content>
            <x-card-header>
                <x-card-title>
                    {{ __('Edit Contact')}}
                </x-card-title>

            </x-card-header>
            <x-card-body>
                <div class="row">
                    <x-input-field :title="__('Address')" name="address" type="text" required col="4" :model="$contact"/>
                    <x-input-field :title="__('Email')" name="email" type="text" required col="4" :model="$contact"/>
                    <x-input-field :title="__('Phone')" name="phone" type="text" required col="4" :model="$contact"/>
                    <x-input-field :title="__('facebook')" name="facebook" type="text" required col="4" :model="$contact"/>
                    <x-input-field :title="__('instagram')" name="instagram" type="text" required col="4" :model="$contact"/>
                    <x-input-field :title="__('youtube')" name="youtube" type="text" required col="4" :model="$contact"/>
                    <x-input-field :title="__('twitter')" name="twitter" type="text" required col="4" :model="$contact"/>

                </div>
            </x-card-body>
            <x-card-footer>
                <x-buttons.update-button/>
            </x-card-footer>
        </x-card-content>
    </form>
@stop
