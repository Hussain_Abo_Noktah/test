@extends('admin.layouts.auth')

@section('auth_content')
    <h3>{{__('Open up your Elahad Account now')}}</h3>
    <p>{{__('Already signed up?')}} <a href="{{ route('school.login') }}">{{__('Log in')}}</a></p>
    <form method="POST" action="{{ route('school.register') }}">
        @csrf
        <div class="form-group">
            <input placeholder="{{__('School Name"')}}" id="name" type="text"
                   class="form-control @error('name') is-invalid @enderror" name="name"
                   value="{{ old('name') }}" required autocomplete="name" autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="email" name="email" id="email" placeholder="{{__('Your email address')}}"
                   class="form-control">
            @error('email')
            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
            @enderror
        </div>
        <div class="form-group">
            <select class="form-control wide" name="governorate_id" required>
                <option value="" selected disabled>{{__('Choose your governorate')}}</option>
                @foreach($governorates as $governorate)
                    <option
                        {{old('governorate_id') == $governorate->id ? 'selected' : ''}}
                        value="{{$governorate->id}}">{{$governorate->title}}</option>
                @endforeach
            </select>
            @error('governorate')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
        <div class="form-group">
            <input placeholder="{{__('Your address')}}" id="address" type="text"
                   class="form-control @error('address') is-invalid @enderror" name="address"
                   value="{{ old('address') }}" required autocomplete="address" autofocus>
            @error('address')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
        <div class="form-group">
            <input placeholder="{{__('Your phone number')}}" id="phone" type="text"
                   class="form-control @error('phone') is-invalid @enderror" name="phone"
                   value="{{ old('phone') }}" required autocomplete="phone" autofocus>
            @error('phone')
            <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
        <div class="form-group">
            <input placeholder="Your fax" id="fax" type="text"
                   class="form-control @error('fax') is-invalid @enderror" name="fax"
                   value="{{ old('fax') }}" required autocomplete="fax" autofocus>
            @error('fax')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="password" name="password" id="password" placeholder="{{__('Create a password')}}"
                   class="form-control">
            @error('password')
            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
            @enderror
        </div>
        <div class="form-group">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                   required autocomplete="new-password" placeholder="{{__('Confirm Your Password')}}">
            @error('password_confirmation')
            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
            @enderror
        </div>
        <button type="submit">{{__('Sign Up')}}</button>
    </form>
@endsection
