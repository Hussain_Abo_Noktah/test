@extends('admin.layouts.auth')

@section('content')
    <div class="login-signin">
        <div class="mb-5">
            <h3>{{__('Sign In To Admin')}}</h3>
            <div class="text-muted font-weight-bold">
                {{__('Login to start your session')}}
            </div>
        </div>
        <form class="form" action="{{route('login')}}" method="post">
            @csrf
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8 @error('username') is-invalid @enderror" type="text" placeholder="{{__('Username')}}" value="test@admin.com" name="username" required autocomplete="off" />
            </div>
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8" type="password" value="12345678" placeholder="{{__('Password')}}" name="password" required/>
            </div>
            <div class="form-group d-flex flex-wrap justify-content-between align-items-center">
                <div class="checkbox-inline">
                    <label class="checkbox m-0 text-muted">
                        <input type="checkbox" name="remember" />
                        <span></span>{{__('Remember me')}}
                    </label>
                </div>
                <a href="{{route('password.request')}}" class="text-muted text-hover-primary">{{__('Forget Password ?')}}</a>
            </div>
            <button type="submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">{{__('Sign In')}}</button>
        </form>
    </div>
@endsection

