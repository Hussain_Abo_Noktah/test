@extends('admin.layouts.auth')

@section('content')
    <div class="login-signin">
        <div class="mb-5">
            <h3>{{ __('Reset Password') }}</h3>
        </div>
        <form class="form" action="{{route('password.email')}}" method="post">
            @csrf
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8 @error('email') is-invalid @enderror" type="email" placeholder="{{__('Email')}}" value="{{old('email')}}" name="email" required autocomplete="off" />
            </div>
            <button type="submit" class="btn btn-primary font-weight-bold px-9 py-4 mx-4">{{__('Send reset link')}}</button>
            <a href="{{route('login')}}" class="btn btn-success font-weight-bold px-9 py-4 mx-4">{{__('Back to login')}}</a>
        </form>
    </div>
@endsection
