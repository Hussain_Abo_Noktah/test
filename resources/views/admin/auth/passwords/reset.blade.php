@extends('admin.layouts.auth')

@section('content')
    <div class="login-signin">
        <div class="mb-5">
            <h3>{{ __('Reset Password') }}</h3>
        </div>
        <form class="form" action="{{route('password.update')}}" method="post">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8 @error('email') is-invalid @enderror" type="text" placeholder="{{__('Email')}}" value="{{old('email', $email)}}" name="email" required autocomplete="off" />
            </div>
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="{{__('Password')}}" name="password" required/>
            </div>
            <div class="form-group mb-5">
                <input class="form-control h-auto form-control-solid py-4 px-8" type="password" placeholder="{{__('Password Confirmation')}}" name="password_confirmation" required/>
            </div>
            <button type="submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4">{{__('Update Password')}}</button>
        </form>
    </div>
@endsection
