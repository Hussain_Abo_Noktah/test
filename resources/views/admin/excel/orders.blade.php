<table>
    <thead>
    <tr>
        <td>{{__('Order Number')}}</td>
        <td>{{ __('Client')}}</td>
        <td>{{__('Phone')}}</td>
        <td>{{__('Address')}}</td>
        <td>{{__('Date')}}</td>
        <td>{{__('Status')}}</td>

    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <td>{{$order->id}}</td>
            <td>{{$order->client->name}}</td>
            <td>{{$order->client->first_phone}}</td>
            <td>{{$order->client->address}}</td>
            <td>{{$order->date}}</td>
            <td>{{$order->status}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
