<table>
    <thead>
    <tr>
        <td>{{__('ID')}}</td>
        <td>{{__('Name')}}</td>
        <td>{{__('Email')}}</td>
        <td>{{__('First Phone')}}</td>
        <td>{{__('Second Phone')}}</td>
        <td>{{__('Address')}}</td>
        <td>{{__('Created At')}}</td>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->first_phone}}</td>
            <td>{{$user->second_phone}}</td>
            <td>{{$user->address}}</td>
            <td>{{$user->created_at}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
