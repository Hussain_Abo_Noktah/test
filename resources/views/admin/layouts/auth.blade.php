<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {!! LaravelLocalization::getCurrentLocaleDirection() == 'rtl' ? 'dir="rtl" style="direction: rtl"' : ''!!}>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <title>{{config('app.name', 'Home Services')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    @include('admin.partials.styles')
    @if(LaravelLocalization::getCurrentLocaleDirection() == 'rtl')
        <link rel="stylesheet" type="text/css" href="{{asset('admin/css/pages/login/classic/login-4.rtl.css')}}" />
    @else
        <link rel="stylesheet" type="text/css" href="{{asset('admin/css/pages/login/classic/login-4.css')}}" />
    @endif
    @yield('styles')
</head>
<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
<div class="d-flex flex-column flex-root">
    <div class="login login-4 login-signin-on d-flex flex-row-fluid">
        <div class="d-flex flex-center flex-row-fluid bgi-size-cover bgi-position-top bgi-no-repeat" style="background-image: url('{{asset('admin/media/bg/bg-3.jpg')}}');">
            <div class="login-form text-center p-7 position-relative overflow-hidden">
                <div class="d-flex flex-center mb-5">
                    <a href="{{url('/')}}">
                        <img src="{{$gs->logo}}" class="max-h-100px" alt="" />
                    </a>
                </div>
                @yield('content')
            </div>
        </div>
    </div>
</div>

@include('admin.partials.scripts')

@include('admin.partials.flashes')

@yield('scripts')

</body>
</html>
