<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {!! LaravelLocalization::getCurrentLocaleDirection() == 'rtl' ? 'direction="rtl" style="direction: rtl"' : ''  !!}>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('app.name')}}</title>
    @include('admin.partials.styles')
    @if(\Request::route()->getName() == 'dashboard')
        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">

    @endif
    @yield('styles')
</head>
<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">

@include('admin.partials.header.mobile-header')

<div class="d-flex flex-column flex-root" id="vue_app">

@include('admin.partials.aside')

    <div class="d-flex flex-row flex-column-fluid page">

        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

        @include('admin.partials.header.desktop-header')

            <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">

            @include('admin.partials.header.sub-header')

                <div class="d-flex flex-column-fluid">

                    <div class="container-fluid">

                        @yield('content')

                    </div>

                </div>

            </div>

            @include('admin.partials.footer')

        </div>

    </div>

</div>

@include('admin.partials.quick.user')

<div id="kt_scrolltop" class="scrolltop">
	<span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
             viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <polygon points="0 0 24 0 24 24 0 24"/>
                <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1"/>
                <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero"/>
            </g>
        </svg>
    </span>
</div>

<div class="page-loader page-loader-base">
    <div class="blockui">
        <span>{{__('Loading ...')}}</span>
        <span>
            <div class="spinner spinner-primary"></div>
        </span>
    </div>
</div>


@include('admin.partials.scripts')

@include('admin.partials.flashes')

@yield('scripts_2')

@yield('scripts')
</body>
</html>
