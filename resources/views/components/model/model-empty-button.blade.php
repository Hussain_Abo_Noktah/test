<a href="{{$route}}" {{ $attributes }} class="btn btn-primary font-weight-bolder">
    <i class="la la-plus"></i>
    {{__('Create New Record')}}
</a>
