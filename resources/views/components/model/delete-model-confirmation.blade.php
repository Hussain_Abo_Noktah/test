<!-- Modal-->
<div class="modal fade" id="delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" class="delete_form" method="post">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">
                        {{__('Delete Confirmation')}}
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body text-center" id="body_overlay">
                    <h4 class="text-danger mb-4">
                        {{__('You are about to delete this record. Everything under this record will be deleted.')}}
                    </h4>
                    <h4 class="text-danger">
                        {{__('Do you want to continue?')}}
                    </h4>
                    <div id="spinner"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-danger font-weight-bold">{{__('Delete')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
