<!--begin::Card-->
<div {{ $attributes->merge(['class' => 'card card-custom']) }}>
    {{$slot}}
</div>
<!--end::Card-->
