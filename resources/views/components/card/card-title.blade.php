<div class="card-title">
    <span class="card-icon">
        <i class="flaticon2-supermarket text-primary"></i>
    </span>
    <h3 class="card-label">
        {{$slot}}
    </h3>
</div>
