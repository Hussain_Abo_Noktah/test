<!--begin: Datatable-->
<table id="datatables" class="table table-bordered table-hover table-checkable" style="margin-top: 13px !important">
    <thead>
    <tr>
        <th>#</th>
        {{$slot}}
        <th>{{__('Control')}}</th>
    </tr>
    </thead>
</table>
<!--end: Datatable-->
