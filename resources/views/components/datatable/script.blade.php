<script>
    $.extend(true, $.fn.dataTable.defaults, {
        "ajax": '{{$route}}',
        columns: [
                @if(isset($enableId) && $enableId === true)
            {
                data: 'id', name: 'id'
            },
                @else
            {
                data: 'DT_RowIndex', name: 'DT_RowIndex'
            },
                @endif
                @foreach($columns as $key => $column)
                @if(is_array($column))
            {
                data: '{{$key}}', name: '{!! $column[0] !!}'
            },
                @else
            {
                data: '{!! $column !!}', name: '{!! $column !!}'
            },
                @endif
                @endforeach
            {
                data: 'action', searchable: false, orderable: false
            }
        ],
    });
</script>
