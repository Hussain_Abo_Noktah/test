<div class="col-md-12 ">
    <div class="card card-language">
        <div class="card-header" style="padding: 14px 25px;">
            <div class="card-toolbar">
                <ul class="nav nav-light-success nav-bold nav-pills">
                    @foreach(LaravelLocalization::getLocalesOrder() as $key => $value)
                        <li class="nav-item">
                            <a class="nav-link {{$loop->first ? 'active' : ''}}" data-toggle="tab"
                               @if(!is_null($id))
                               href="#{{$id}}_language_{{$key}}"
                               @else
                               href="#language_{{$key}}"
                                @endif
                            ><span class="nav-icon"><i class="flaticon2-chat-1"></i></span>
                                <span class="nav-text">{{$value['name']}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="card-body">
            <div class="tab-content">
                {{$slot}}
            </div>
        </div>
    </div>
</div>
