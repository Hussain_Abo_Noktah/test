<div {{$attributes->merge(['class' => 'form-group', 'style' => ''])}}>
    <label for="image">{{$title ?? __('Upload New Image')}} @if($required == true) <sup>*</sup> @endif</label>
    @if(isset($model) && $model->hasMedia($collection))
        <sup>
            <a href="javascript:;" data-toggle="modal" data-target="#show_model_{{$collection}}">
                {!! \App\Support\Icons::ShowIcon() !!}
            </a>
        </sup>
    @endif
    <div class="custom-file">
        <input type="file"
               @if($multi == true)
               name="{{$name . '[]' ?? 'image[]'}}"
               @else
               name="{{$name ?? 'image'}}"
               @endif
               class="custom-file-input" id="{{$name ?? 'image'}}" @if($multi == true) multiple
               @endif @if($required == true) required @endif>
        <label class="custom-file-label" for="{{$name ?? 'image'}}">{{ $subTitle ?? __('Upload Image')}}</label>
    </div>
</div>

@if(isset($model) && $model->hasMedia($collection))
    <!-- Modal-->
    <div class="modal fade" id="show_model_{{$collection}}" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('Show Files')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @if($model->hasMedia($collection))
                            @foreach($model->getMedia($collection) as $file)
                                @if(in_array($file->mime_type, \App\Models\Media::$AUDIOMIMESTYPES))
                                    <audio controls style="width: 100%">
                                        <source src="{{$file->getUrl()}}" type="{{$file->mime_type}}">
                                        Your browser does not support the audio tag.
                                    </audio>
                                @endif
                                @if(in_array($file->mime_type, \App\Models\Media::$IMAGESMIMESTYPES))
                                    <div class="col-md-6">
                                        <img src="{{$file->getUrl()}}" alt="{{$collection}}" class="img-fluid">
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold"
                            data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endif
