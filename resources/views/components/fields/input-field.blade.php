<div  {{$attributes->merge(['style' => '', 'class' => 'col-md-' . ($col ?? '4') . ' form-group '])}}>
    @if($type !== "hidden")
    <label for="{{$name}}">{{$title}} {!!  $required ? '<sup>*</sup>' : '' !!}</label>
    @endif
    <input type="{{$type ?? 'text'}}" name="{{$name}}" value="{{old($name, $model ? $model->$name : '')}}" class="form-control {{$name}} @error($name) is-invalid @enderror" placeholder="{{$title}}" {{$required ? 'required' : ''}} id="{{$name}}" />
    @error($name)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
