<div class="col-md-{{$col ?? '4'}} form-group" {{$attributes->merge(['style' => ''])}}>
    <label for="{{$name}}">{{__($title)}} {!! $required ? '<sup>*</sup>' : '' !!}</label>
    <select name="{{$name}}" class="form-control select2" {{$required ? 'required' : ''}} id="{{$name}}">
        @if($required)
            <option value="" selected disabled>{{__('Choose An Option')}}</option>
        @endif
        @if($all)
            <option value="all" selected>{{__('All')}}</option>
        @endif
        @foreach($selectBoxData as $singleData)
            <option
                @if(old($name))
                {{old($name) == $singleData->$value ? 'selected' : ''}}
                @elseif($model != null)
                @if($secondName != null)
                {{$model->$secondName == $singleData->$value ? 'selected' : ''}}
                @else
                {{$model->$name == $singleData->$value ? 'selected' : ''}}
                @endif
                @endif
                value="{{$singleData->$value}}">{{$singleData->$valueData}}</option>
        @endforeach
    </select>
</div>
