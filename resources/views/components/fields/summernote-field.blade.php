<div class="col-md-12">
    <div class="form-group">
        <label for="{{$name}}_{{$index}}">{{$title}} @if(isset($required) && $required == true) <sup>*</sup> @endif
        </label>
        <textarea
            id="{{$name}}_{{$index}}"
            type="text"
            name="{{$name}}[{{$locale}}]"
            class="form-control summernote"
            placeholder="{{$title}}"
            @if(isset($required) && $required == true) required @endif
        >{{old($name . '.' . $locale, isset($model) ? $model->getTranslation($name, $locale) : null)}}</textarea>
    </div>
</div>
