const chatList = document.querySelector('#messages-data');
const form = document.querySelector('#send-message-form');
const CONVERSATIONS = 'conversations';
const MESSAGES = 'messages';
const db = firebase.firestore();
const NO_MESSAGE = '<h2 class="text-center mb-0" id="no-message">There is no message</h2>';
const doneTypingInterval = 3000;
const $input = $('#content');
let typingTimer;


db.settings({
    timestampsInSnapShot: true,
})


function renderMessages(doc) {
    let content = doc.data().content;
    let name = '',
        profile_image = '',
        flex_direction = '',
        image_direction = '',
        color = '';
    if (parseInt(doc.data().sender_id) === parseInt(CURRENT_SENDER_ID)) {
        flex_direction = 'end';
        image_direction = 'left';
        name = CURRENT_SENDER_NAME;
        profile_image = CURRENT_SENDER_IMAGE_URL;
        color = 'primary';
    } else {
        flex_direction = 'start';
        image_direction = 'right';
        name = CURRENT_RECEIVE_NAME;
        profile_image = CURRENT_RECEIVER_IMAGE_URL;
        color = 'success';
    }
    let message = '';
    message = '<div data-id="' + doc.id + '" class="d-flex flex-column mb-5 align-items-' + flex_direction + '">';
    message += '<div class="d-flex align-items-center">';
    if (parseInt(doc.data().sender_id) === parseInt(CURRENT_SENDER_ID)) {
        message += '<div>';
        message += '<span class="text-muted font-size-sm">' + moment(doc.data().sent_at, 'x').format("DD MMM YYYY hh:mm a") + '</span>';
        message += '<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6 ml-2">' + name + '</a>';
        message += '</div>';
        message += '<div class="symbol symbol-circle symbol-40 ml-1">';
        message += '<img alt="Pic" src="' + profile_image + '"/>';
        message += '</div>';
    } else {
        message += '<div class="symbol symbol-circle symbol-40 mr-1">';
        message += '<img alt="Pic" src="' + profile_image + '"/>';
        message += '</div>';
        message += '<div>';
        message += '<a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6 mr-2">' + name + '</a>';
        message += '<span class="text-muted font-size-sm">' + moment(doc.data().sent_at, 'x').format("DD MMM YYYY hh:mm a") + '</span>';
        message += '</div>';
    }
    message += '<div>';
    message += '</div>';
    message += '</div>';
    message += '<div class="mt-2 rounded p-5 bg-light-' + color + ' text-dark-50 font-weight-bold font-size-lg text-' + image_direction + ' max-w-600px">';
    message += content;
    message += ' </div>';
    message += ' </div>';
    chatList.innerHTML += message;
}


function submitForm() {
    if (form.content.value === "") {
        toastr.error('Please write your message');
        return false;
    } else {
        db.collection(CONVERSATIONS).doc(CURRENT_DOC).collection(MESSAGES).add({
            content: form.content.value,
            sender_id: parseInt(CURRENT_SENDER_ID),
            seen: true,
            sent_at: parseInt(moment().format('x')),
        });

        $.ajax({
            url: '/send-notification/' + window.RECEIVERDATA.id,
            data: {
                body: form.content.value,
            },
            dataType: "json",
            success: function (response) {
            }
        })

        form.content.value = '';

        db.collection(CONVERSATIONS).doc(CURRENT_DOC).collection("users").doc(CURRENT_SENDER_ID).set({
            is_typing: false,
        });

        db.collection(CONVERSATIONS).doc(CURRENT_DOC).collection("users").doc(CURRENT_RECEIVE_ID).set({
            is_typing: false,
        });

        db.collection("users").doc(CURRENT_SENDER_ID).set({
            fcmToken: window.SENDERDATA.device_token,
            id: window.SENDERDATA.id,
            name: window.SENDERDATA.name,
            phone: window.SENDERDATA.phone,
            profile: CURRENT_SENDER_IMAGE_URL,
        });


        db.collection("users").doc(CURRENT_SENDER_ID).collection('contacts').doc(CURRENT_RECEIVE_ID).get().then(doc => {
            if (!doc.exists) {
                db.collection("users").doc(CURRENT_SENDER_ID).collection('contacts').doc(CURRENT_RECEIVE_ID).set({
                    chat_created_at: parseInt(moment().format('x')),
                    conversation_id: CURRENT_DOC,
                });
            }
        });

        db.collection("users").doc(CURRENT_SENDER_ID).collection('contacts').doc(CURRENT_RECEIVE_ID).get().then(doc => {
            if (!doc.exists) {
                db.collection("users").doc(CURRENT_RECEIVE_ID).collection('contacts').doc(CURRENT_SENDER_ID).set({
                    chat_created_at: parseInt(moment().format('x')),
                    conversation_id: CURRENT_DOC,
                });
            }
        });

        db.collection("users").doc(CURRENT_RECEIVE_ID).set({
            fcmToken: window.RECEIVERDATA.device_token,
            id: window.RECEIVERDATA.id,
            name: window.RECEIVERDATA.name,
            phone: window.RECEIVERDATA.phone,
            profile: CURRENT_RECEIVER_IMAGE_URL,
        });


    }
}

form.addEventListener('submit', (e) => {
    e.preventDefault();
    submitForm();
});


db.collection(CONVERSATIONS).doc(CURRENT_DOC).collection(MESSAGES).orderBy('sent_at').onSnapshot(snapshot => {
    let changes = snapshot.docChanges();
    if (changes.length === 0) {
        chatList.innerHTML = NO_MESSAGE;
    } else {
        changes.forEach(change => {
            let no_message = chatList.querySelector('#no-message') || false;
            if (no_message) {
                chatList.removeChild(no_message);
            }
            renderMessages(change.doc);
        });
    }
})


$input.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

$input.on('keydown', function () {
    db.collection(CONVERSATIONS).doc(CURRENT_DOC).collection("users").doc(CURRENT_SENDER_ID).set({
        is_typing: false,
    });
    clearTimeout(typingTimer);
});

function doneTyping() {
    db.collection(CONVERSATIONS).doc(CURRENT_DOC).collection("users").doc(CURRENT_SENDER_ID).set({
        is_typing: true,
    });
}
