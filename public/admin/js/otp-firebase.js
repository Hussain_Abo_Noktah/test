$(function () {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {'size': 'invisible'});

    $('#sendCode').on('click', function (e) {
        let phone = $('#first_phone').val();
        let appVerifier = window.recaptchaVerifier;
        firebase.auth().signInWithPhoneNumber(phone, appVerifier)
            .then(function (confirmationResult) {
                window.confirmationResult = confirmationResult;
                coderesult = confirmationResult;
                console.log(coderesult);
            }).catch(function (error) {
            console.log(error.message);
        });
    });

    $('#verifyPhone').on('click', function () {
        var code = $('#code').val();
        $(this).attr('disabled', 'disabled');
        $(this).text('Processing..');
        confirmationResult.confirm(code).then(function (result) {
            var user = result.user;
            console.log(user);
        }.bind($(this))).catch(function (error) {
            $(this).removeAttr('disabled');
            $(this).text('Invalid Code');
            setTimeout(() => {
                $(this).text('Verify Phone No');
            }, 2000);
        }.bind($(this)));

    });
});

// <a href="javascript:;" className="btn btn-success" id="sendCode">{{__('Send Code')}}</a>
// <input type="text" className="form-control" id="code">
//     <a href="javascript:;" className="btn btn-success" id="verifyPhone">{{__('verify Phone')}}</a>
// <div className="col-md-12">
//     <div id="recaptcha-container"></div>
// </div>
