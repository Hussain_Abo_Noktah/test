/**
 * Saphir Website
 * Version: 1.1
 * Powerd by:
 * Designer: Ayat Ahmed | instagram.com/ayaat.a7med
 * Copyrights 2021
 */
$(document).ready(function () {



    "use strict";
    var clnd = new Calendar({
        id: '#pickDate',

        primaryColor: '#0A9973',
        headerColor: '#0A9973',

        dateChanged: function (data , events) {
            const events_display = document.querySelector('.events-display');
            let events_html = '';


            events.forEach(event => {
                events_html += `
        <div class="event col-lg-6">
          <div class="event-left">
            <div class="event-title mb-2">${event.startTime}</div>
             <div class="event-title">${event.endTime}</div>
         
          </div>
          <div class="event-right ">
          
                       <div class="form-group gr-button mb-2"><button type="submit" onclick=function putAppointment(EXTERNAL_FRAGMENT) {
                       
                       }
                       "putAppointment(${event.id})" class="w-75 btn btn-success"
                                                 >${event.button}</button></div>
          </div>
        </div>
      `
            });
            if(events_html) {
                events_display.innerHTML = events_html;
            } else {
                events_display.innerHTML = '<div class="no-events-text">No events on this day :(</div>';
            }
            var da = new Date(data),
                weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
                d = da.getDate(),
                dn = weekday[da.getDay()],
                m = (da.getMonth() + 1),
                y = da.getFullYear(),
                ald = d + "-" + m + "-" + y;
            $('#pickedDate').val(ald);
            $('#viewPickedDate').html('<span class="second-text">'+ dn +':</span> ' + ald);
        },
        selectedDateClicked: function (date) {
            clnd.reset();
        }
    });
    fetch('/events').then(res => res.json()).then(data => {


        clnd.setEventsData(data);
        if(Object.keys(clnd.eventDayMap).length === 0) {
            clnd.updateCurrentDate(1);
        }
    });
    /*
     $(document).on('each','.color-calendar.basic .calendar__days .calendar__day', function () {
     $(this).css({'height': $(this).width()});
     });*/


    function putAppointment(id) {
        alert(id)
    }

});
