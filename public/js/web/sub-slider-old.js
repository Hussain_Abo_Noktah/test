/**
 * Saphir Website
 * Version: 1.1
 * Powerd by:
 * Designer: Ayat Ahmed | instagram.com/ayaat.a7med
 * Copyrights 2021
 */
/*
*
 .sub-slider {
 #all-container {
 padding: 0;
 margin: 0;
 width: 100%;
 height: 100vh;
 -webkit-perspective: 1000;
 perspective: 1000;
 .all-slides {
 @extend .pos-absolute;
 width: 400%;
 height: 100%;
 .sd-item {
 @extend .pos-absolute;
 height: 100%;
 width: 25%;
 }
 }
 }
 }
*
*
*
*
* */
$(document).ready(function () {
    "use strict";
    var controller = new ScrollMagic.Controller();

    // define movement of panels
    var wipeAnimation = new TimelineMax()
    // animate to second panel
        .to("#sub-slider .all-slides", 0.5, {z: -150})		// move back in 3D space
        .to("#sub-slider .all-slides", 1,   {x: "-25%"})	// move in to first panel
        .to("#sub-slider .all-slides", 0.5, {z: 0})				// move back to origin in 3D space
        // animate to third panel
        .to("#sub-slider .all-slides", 0.5, {z: -150, delay: 1})
        .to("#sub-slider .all-slides", 1,   {x: "-50%"})
        .to("#sub-slider .all-slides", 0.5, {z: 0})
        // animate to forth panel
        .to("#sub-slider .all-slides", 0.5, {z: -150, delay: 1})
        .to("#sub-slider .all-slides", 1,   {x: "-75%"})
        .to("#sub-slider .all-slides", 0.5, {z: 0});

    // create scene to pin and link animation
    new ScrollMagic.Scene({
        triggerElement: "#all-container",
        triggerHook: "onLeave",
        duration: "500%"
    })
        .setPin("#all-container")
        .setTween(wipeAnimation)
        //.addIndicators() // add indicators (requires plugin)
        .addTo(controller);

});