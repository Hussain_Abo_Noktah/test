/**
 * Saphir Website
 * Version: 1.1
 * Powerd by:
 * Designer: Ayat Ahmed | instagram.com/ayaat.a7med
 * Copyrights 2021
 */
$(document).ready(function () {
    "use strict";
    var subSlider=$('.all-slides').slick({
        arrows: false,
        dots: false,
        autoplay: true,
        infinite: false,
        rtl:true,
        speed: 300,
        slidesToShow: 1,
        centerMode: false,
        adaptiveHeight: true
    });

});