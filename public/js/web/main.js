/**
 * Saphir Website
 * Version: 1.1
 * Powerd by:
 * Designer: Ayat Ahmed | instagram.com/ayaat.a7med
 * Copyrights 2021
 */
$(document).ready(function () {
    function responsiveFunc() {

    }
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl, {
            container: 'body'
        })
    });
    function notScolledBar() {
        var t = $('#topbar');
        t.removeClass('scrolling');
        if(t.is('.darkbar')){
            t.removeClass('lighter');
        }
    }

    function scolledBar() {
        var t = $('#topbar');
        t.addClass('scrolling');
        if(t.is('.darkbar')){
            t.addClass('lighter');
        }

    }

    function readFileURL(input, output) {
        if (input[0].files[0]) {
            var f = input[0].files[0];
            output.text(f.name);
        }
    }
    AOS.init({
        once: true,
        mirror: true,
        duration: 1000,
        disable: 'mobile'
    });
    $(window).on('load scroll resize', function () {

        var wt = $(window).scrollTop(),
            ww = $(window).width(),
            wts = (wt > 150),
            wws = (ww < 1200);
        if (wts || wws) {
            scolledBar();
            if (wts) {
                $('#topbar').addClass('main-bg')
            } else {
                $('#topbar').removeClass('main-bg')
            }

        } else {
            notScolledBar();
            if (!wts) {
                $('#topbar').removeClass('main-bg')
            }
        }
        if(wws && !wts){
            $('.darkbar').removeClass("lighter")
        }
    });
    $('.arrows .next').on('click', function () {
        $(this).parents('section').find('.carousel ').carousel('next');
        $(this).parents('section').find('.slick-slider ').slick('slickNext');
    });
    $('.arrows .prev').on('click', function () {
        $(this).parents('section').find('.carousel ').carousel('prev');
        $(this).parents('section').find('.slick-slider ').slick('slickPrev');
    });
    $(document).on('click','.poVid',function() {
        var ssr = $(this).attr("data-src"),
            isYou = $(this).attr("data-youtube"),
            popVid = $('#pop-player');

        if(isYou == "true") {
            var ell = '<iframe width="100%" src="'+ ssr +'"></iframe>';
        }else{
            var ell = '<video id="pop-player" class="video-js vjs-matrix h-100 w-100" autoplay controls  preload="auto"> <source src="' + ssr + '" type="video/mp4" id="src-mp4"></source></video>';
        }

        $('#popupVid').find('.content').html(ell);
        $('#popupVid').fadeIn(500);
    });
    $(document).on('click','.poImg',function() {
        var sr = $(this).attr("data-src"), el = "";
        el = "<img  src='" + sr + "' class='d-block m-auto'/>";
        $("#popupimg .content").prepend(el);
        $("#popupimg").fadeIn(500);
    });

    // pop up setting
    $('.closeView').click(function () {
        $(this).parents('.popup').fadeOut(500);
        $(this).parents('.popup').find("video ,iframe").remove();
        $(this).parents('.popup').find("img").removeAttr('src');
    });

    $('.open-menu').on('click', function () {
        var t = $(this).attr('data-target'),
            c = $('#topbar .open-menu'),
            d = $('#topbar.darkbar');
        $(t).toggleClass('show');
        c.toggleClass('active light');
          d.toggleClass('not-darker');

    });
    $('.close-menu, .top-menu a').on('click', function () {
        $(this).parents('.top-menu').removeClass('show');
    });
    $(window).on('resize load scroll', function () {
        responsiveFunc();
    });

    $('.request-projectt .form-control').on('keyup keypress click', function () {
        var v = $(this).val();
        if((v !== "") && (v !== " ") && (v != undefined)){
            $(this).addClass('not-empty');
        }else{
            $(this).removeClass('not-empty');
        }
    });
    $('.serv-item').on('click', function () {
       if($(this).is('.active')) {
           $(this).removeClass('active');
       }else{
           $(this).addClass('active');
           $('.serv-item').not($(this)).removeClass('active');
       }
    });    $(document).on('change', '.file-wrap input[type=file]', function () {
        var nm = $(this).parents('.file-wrap').find('.file-name');
        readFileURL($(this), nm);
        $('.delete-icon').removeClass('d-none').addClass('d-inline-block');
    });
    $('.delete-icon').on('click', function () {
        var nm = $(this).parents('.file-wrap').find('.file-name');
        $(this).parents('.file-wrap').find('input[type=text]').val('');
        $(this).addClass('d-none').removeClass('d-inline-block');
        nm.text('');
    });
    var sp = document.getElementById('selectPaid');
    if(sp) {
        $('#selectPaid').on('change', function () {
            var op = $('option:selected', this),
                par = op.attr('data-parent'),
                tab = op.attr('data-tab');

            $(par).find('.paid-tab').not(tab).removeClass('active');
            $(tab).addClass('active');
        });
    }
    $('.select-wrap select').on(
        {'focus': function () {
        $(this).parents('.select-wrap').addClass('focused')
        },
        'change': function () {
            $(this).blur();
        },
         'blur':function () {
             $(this).parents('.select-wrap').removeClass('focused')
         }
        }
    )
});