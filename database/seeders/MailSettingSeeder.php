<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MailSettingSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('settings')->delete();
        \DB::table('settings')->insert(array(
            array(
                'id' => 1,
                'key' => 'mail_driver',
                'value' => 'smtp',
            ),
            array(
                'id' => 2,
                'key' => 'mail_host',
                'value' => 'smtp.hostinger.com',
            ),
            array(
                'id' => 3,
                'key' => 'mail_port',
                'value' => '587',
            ),
            array(
                'id' => 4,
                'key' => 'mail_username',
                'value' => 'fooddelivery@smartersvision.com',
            ),
            array(
                'id' => 5,
                'key' => 'mail_password',
                'value' => 'data',
            ),
            array(
                'id' => 6,
                'key' => 'mail_encryption',
                'value' => 'ssl',
            ),
            array(
                'id' => 7,
                'key' => 'mail_from_address',
                'value' => 'fooddelivery@smartersvision.com',
            ),
            array(
                'id' => 8,
                'key' => 'mail_from_name',
                'value' => 'Smarter Vision',
            ),
            array(
                'id' => 9,
                'key' => 'mailgun_domain',
                'value' => 'mail@example.com',
            ),  array(
                'id' => 10,
                'key' => 'mailgun_secret',
                'value' => 'diee',
            ),  array(
                'id' => 11,
                'key' => 'postmark_token',
                'value' => 'diee',
            ),
            array(
                'id' => 12,
                'key' => 'imap_mail_host',
                'value' => 'smtp.hostinger.com',
            ),
            array(
                'id' => 13,
                'key' => 'imap_mail_port',
                'value' => '587',
            ),
            array(
                'id' => 14,
                'key' => 'imap_mail_username',
                'value' => 'fooddelivery@smartersvision.com',
            ),
            array(
                'id' => 15,
                'key' => 'imap_mail_password',
                'value' => 'data',
            ),
            array(
                'id' => 16,
                'key' => 'imap_mail_encryption',
                'value' => 'ssl',
            ),

        ));


    }
}
